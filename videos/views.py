from django.views.generic import ListView
from .models import VideoPost


class VideoList(ListView):
    template_name = "videos/videos_list.html"
    context_object_name = "videos"
    model = VideoPost
    paginate_by = 24
