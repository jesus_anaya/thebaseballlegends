from django.contrib import admin
from django.utils.safestring import mark_safe
from .models import VideoPost


class VideoPostAdmin(admin.ModelAdmin):
    fieldsets = (
        ('', {
            'fields': ('title', 'enable'),
        }),
        ('Video URL', {
            'classes': ('grp-collapse grp-open',),
            'fields' : ('video', 'thumbnail'),
        }),
        ('Optional Comment', {
            'classes': ('grp-collapse grp-open',),
            'fields' : ('comment',),
        })
    )

    list_display = ('video_thumbnail', 'title', 'enable', 'created')
    list_editable = ('enable',)

    def video_thumbnail(self, obj):
        return mark_safe("<image src='%s' width='100' height='70'>" \
                                            % obj.get_thumbnail())


admin.site.register(VideoPost, VideoPostAdmin)
