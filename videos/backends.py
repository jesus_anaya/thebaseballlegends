from embed_video.backends import VideoBackend
import re


class MLBBackend(VideoBackend):
    re_detect = re.compile(r'http://wapc\.mlb\.com/play/?\?content_id=[0-9]+')
    re_code = re.compile(r'http://wapc\.mlb\.com/play/?\?content_id=(?P<code>[0-9]+)')

    allow_https = False
    pattern_url = '{protocol}://wapc.mlb.com/play?content_id={code}'

    def get_thumbnail_url(self):
        return ""

    def get_url(self):
        url = "http://wapc.mlb.com/shared/video/embed/embed.html?content_id="
        return "%s%s&width=720&height=407&property=mlb" % (url, self.code)
