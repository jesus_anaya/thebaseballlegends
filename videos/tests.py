"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from django.core.urlresolvers import reverse
import factory
from .models import VideoPost


class VideoPostFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = VideoPost

    title = "Title video post test"
    enable = True
    video = "http://www.youtube.com/watch?v=W8CuRtCHWD8"
    comment = "Comment test"
    thumbnail = factory.django.ImageField()


class SimpleTest(TestCase):
    def test_correct_save_video(self):
        """
        """
        video = VideoPostFactory.create()
        self.assertTrue(video.id is not None)

        video2 = VideoPostFactory.create(thumbnail=None)
        self.assertTrue(video2.id is not None)

        video3 = VideoPostFactory.create(comment=None)
        self.assertTrue(video3.id is not None)

    def test_views(self):
        response = self.client.get(reverse("videos_list"))
        self.assertEquals(response.status_code, 200)