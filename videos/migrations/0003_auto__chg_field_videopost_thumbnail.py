# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'VideoPost.thumbnail'
        db.alter_column(u'videos_videopost', 'thumbnail', self.gf('stdimage.fields.StdImageField')(max_length=100, null=True, upload_to='videos/thumbnails/', size={'width': 170, 'force': None, 'height': 188}))

    def backwards(self, orm):

        # Changing field 'VideoPost.thumbnail'
        db.alter_column(u'videos_videopost', 'thumbnail', self.gf('stdimage.fields.StdImageField')(size={'width': 170, 'force': None, 'height': 188}, default=None, max_length=100, upload_to='videos/thumbnails/'))

    models = {
        u'videos.videopost': {
            'Meta': {'ordering': "('-created',)", 'object_name': 'VideoPost'},
            'comment': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'enable': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'thumbnail': ('stdimage.fields.StdImageField', [], {'blank': 'True', 'max_length': '100', 'null': 'True', 'upload_to': "'videos/thumbnails/'", 'size': "{'width': 170, 'force': None, 'height': 188}"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'video': ('embed_video.fields.EmbedVideoField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['videos']