from django.conf.urls import patterns, url
from .views import VideoList

urlpatterns = patterns('',
    url(r"^$", VideoList.as_view(), name="videos_list")
)