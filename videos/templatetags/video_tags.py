from django import template
from videos.models import VideoPost
register = template.Library()


@register.inclusion_tag('videos/video_slider.html')
def render_video_slider():
    """
    Render to last 10 video in the home videos slider
    """
    return {'videos': VideoPost.objects.all()[:10]}


@register.inclusion_tag('videos/video_popup_templates.html')
def render_video_popup_templates(videos=None):
    """
    Render to last 10 video in the home videos slider
    """
    if not videos:
        videos = VideoPost.objects.all()[:10]

    return {'videos': videos}
