from django.db import models
from django.core.urlresolvers import reverse_lazy
from stdimage import StdImageField
from embed_video.fields import EmbedVideoField
from embed_video.backends import detect_backend
from website.utils import unique_slug


class VideoPost(models.Model):
    class Meta:
        verbose_name = 'Video'
        verbose_name_plural = 'Videos'
        ordering = ('-created',)

    title = models.CharField(max_length=200)
    enable = models.BooleanField(default=True, blank=True)

    video = EmbedVideoField()
    thumbnail = StdImageField(upload_to="videos/thumbnails/", blank=True,
                    null=True, size=(170, 188), thumbnail_size=(170, 188))

    comment = models.TextField(blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.title

    def get_thumbnail(self):
        print "self.thumbnail", self.thumbnail
        if self.thumbnail:
            return self.thumbnail.url
        else:
            return detect_backend(str(self.video)).get_thumbnail_url()
