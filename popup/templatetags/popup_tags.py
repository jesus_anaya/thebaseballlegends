from django import template
from django.template.loader import render_to_string
from popup.models import Popup
from datetime import date
register = template.Library()


@register.simple_tag
def home_popup():
    try:
        popup = Popup.objects.filter(start_date__lte=date.today(),
                                    end_date__gte=date.today())[0]
        return render_to_string("popup/popup.html", {
            'popup': popup
        })
    except IndexError:
        return ""
