from django.contrib import admin
from .models import Popup


class PopupAdmin(admin.ModelAdmin):
    pass


admin.site.register(Popup, PopupAdmin)
