from django.db import models
from stdimage import StdImageField


class Popup(models.Model):
    title = models.CharField(max_length=255)
    image = StdImageField(upload_to="popups/", blank=True,
            size=(600, 410), thumbnail_size=(600, 410, True))
    url = models.URLField(blank=True, null=True)

    # Button link
    whos_comming = models.URLField(blank=True, null=True)
    more_info = models.URLField(blank=True, null=True)
    watch_game_live = models.URLField(blank=True, null=True)

    start_date = models.DateField()
    end_date = models.DateField()

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return self.url
