# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Popup'
        db.create_table(u'popup_popup', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('image', self.gf('stdimage.fields.StdImageField')(blank=True, max_length=100, upload_to='popups/', thumbnail_size={'width': 600, 'force': True, 'height': 410}, size={'width': 600, 'force': None, 'height': 410})),
            ('url', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
            ('whos_comming', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
            ('more_info', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
            ('watch_game_live', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
            ('start_date', self.gf('django.db.models.fields.DateField')()),
            ('end_date', self.gf('django.db.models.fields.DateField')()),
        ))
        db.send_create_signal(u'popup', ['Popup'])


    def backwards(self, orm):
        # Deleting model 'Popup'
        db.delete_table(u'popup_popup')


    models = {
        u'popup.popup': {
            'Meta': {'object_name': 'Popup'},
            'end_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('stdimage.fields.StdImageField', [], {'blank': 'True', 'max_length': '100', 'upload_to': "'popups/'", 'thumbnail_size': "{'width': 600, 'force': True, 'height': 410}", 'size': "{'width': 600, 'force': None, 'height': 410}"}),
            'more_info': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'start_date': ('django.db.models.fields.DateField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'watch_game_live': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'whos_comming': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['popup']