$(window).resize(function()
{
	detectResponsiveScreen();
});

function detectResponsiveScreen()
{
	var window_width = $(window).width();
	if(window_width <= 480)
	{
		$(".front-popup").css({"visibility": "hidden", "display": "none"});
		$("#standings").css({"max-width": "none"});
		$("#scores").css({"max-width": "auto"});
		$("#bg").css({"background-position-x": "1000px"});
	}
	else
	{
		$("#standings").css({"max-width": "185"});
		$("#scores").css({"max-width": "185"});
		$("#bg").css({"background-position-x": "center"});
	}
}

function socialFooter(foot_item)
{
	var items = ["facebook", "instagram", "plus", "pinterest", "mail-footer"];
	
	for(var item = 0; item < items.length; item++)
	{
		if(items[item] == foot_item)
		{
			$("#" + items[item]).css({"visibility": "visible", "z-index": "10"});
			$("#item_" + items[item]).addClass("social-active");
		}
		else
		{
			$("#" + items[item]).css({"visibility": "hidden", "z-index": "1"});
			$("#item_" + items[item]).removeClass("social-active");
		}
	}
}

function searchInputEffect()
{
	if($("#search-input").val() == "Search")
	{
		$("#search-input").val("");
	}
}

function sidepanelEffect()
{
	var side_left = $("#sidepanel-left");
	var side_right = $("#sidepanel-right");
	var windows_top = $(window).scrollTop();

	if(windows_top >= 150)
	{
		side_left.css({"position": "fixed", "top": "0px"});
		side_right.css({"position": "fixed", "top": "0px"});
	}
	else
	{
		side_left.css({"position": "absolute", "top": "150px"});
		side_right.css({"position": "absolute", "top": "150px"});
	}
}

function openPhotoPopup(popup_id)
{
	$(popup_id).css(
	{
		'visibility': 'visible',
		'display': 'inline'
	});
}

function closePhotoPopup(popup_id)
{
	$(popup_id).css(
	{
		'visibility': 'hidden',
		'display': 'none'
	});
}

$(document).ready(function()
{
	$(window).scroll(function() 
	{
		sidepanelEffect();
	});

	$("body").css('visibility', 'visible');

	detectResponsiveScreen();

	/* //debug
	$(document).bind("contextmenu", function(e)
	{
		e.preventDefault();
    });
	*/

	// limit chars size
	/*$('.limit-text-size').text(function() 
	{
		var text = $(this).html();
		var limit = 250;

		if(text.length > limit)
		{
	    	return text.substring(0, text.indexOf(' ', limit - 10)) + "...";
	    }
	    return text;
	});
*/
});
