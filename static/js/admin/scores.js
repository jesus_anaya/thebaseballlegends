(function($)
{
    $(window).load(function()
    {
        function get_team_list()
        {
            var url_request = "?";

            /*if($("#id_state").val() !== ''){
                url_request += "state=" + $("#id_state").text() + "&";
            }*/

            if($("#id_team_divi").val() !== ''){
                url_request += "division=" + $("#id_team_divi").val() + "&";
            }

            if($("#id_age_group").val() !== ''){
                url_request += "age_group=" + $("#id_age_group").val() + "&";
            }

            if($("#id_is_tournament").is(':checked')){
                if($("#id_tournament option:selected").val() !== ''){
                    url_request += "tournament=" + $("#id_tournament option:selected").text() + "&";
                }
            } else if($("#id_is_world").is(':checked')) {
                if($("#id_world option:selected").val() !== ''){
                    url_request += "tournament=" + $("#id_world option:selected").text() + "&";
                }
            }

            url_request_team_1 = url_request + 'selected=' + $("#id_team_1").val() + '&';
            $.get("/admin/teams_filter/" + url_request_team_1).done(function(data){
                $('#id_team_1').html(data);
            });

            url_request_team_2 = url_request + 'selected=' + $("#id_team_2").val() + '&';
            $.get("/admin/teams_filter/" + url_request_team_2).done(function(data){
                $('#id_team_2').html(data);
            });
        }

        $("#id_team_divi").on('change', get_team_list);
        $("#id_age_group").on('change', get_team_list);
        $("#id_tournament").on('change', get_team_list);
        $("#id_world").on('change', get_team_list);
        get_team_list();

    });
})
(grp.jQuery);