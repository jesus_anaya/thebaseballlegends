(function($) {
    function clean_checkboxes(item) {
        $('input[type="checkbox"]').each(function(){
            if($(this).attr('name').search('is_front') !== -1) {
                if(this != item) {
                    $(this).attr('checked', false);
                }
            }
        });
    }

    $(document).ready(function(){
        $('input[type="checkbox"]').each(function(){
            if($(this).attr('name').search('is_front') !== -1) {
                $(this).click(function(){
                    clean_checkboxes(this);
                });
            }
        });

        var gallery = parseInt(window.location.href.match('/[0-9]+/')[0].replace('/', ''), 10);
        $("#gallery-id").val(gallery);

        Dropzone.options.galleryDropzone = {
            autoProcessQueue: true,
            paramName: 'image'
        };

    });
})
(django.jQuery);

