function world_checkbox() {
    if($("#id_tournament_type").val() != 1 /* World*/) {
        $("#id_view_in_front").attr('disabled', true);
    } else {
        $("#id_view_in_front").attr('disabled', false);
    }
}

$(document).ready(function(){
    $("#id_tournament_type").change(function(){
        world_checkbox();
    })
    .trigger('change');
});