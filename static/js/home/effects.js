
$("#select_score").change(function ()
{
    var name = $("#select_score option:selected").val();
    $.get("/home_score/?name=" + name, function(data)
    {
		$("#home-scores").html(data);
	});

	$("#link-to-scores").attr('href', '/scores/?menu-select=' + name);
});

$("#select_standing").change(function ()
{
    var name = $("#select_standing option:selected").val();
    $.get("/home_standing/?name=" + name, function(data)
    {
		$("#home-standings").html(data);
	});

	$("#link-to-standings").attr('href', '/standings/?menu-select=' + name);
});

(function() {

	// Home video slide
    $(".video-slider").css('display', 'inline-block');

    $("#slider").easySlider({
        auto: false,
        continuous: false,
        controlsShow: true,
        prevId: 'prev-arrow-video',
        nextId: 'next-arrow-video'
    });

    // News post slide
    $('#slides').slides(
    {
        preload: true,
        preloadImage: '/static/img/loading.gif',
        play: 5000,
        pause: 2500,
        hoverPause: true,
        animationStart: function(current)
        {
            $('.caption').animate({bottom: -35}, 100);
        },
        animationComplete: function(current)
        {
            $('.caption').animate({bottom: 0}, 200);
        },
        slidesLoaded: function()
        {
            $('.caption').animate({bottom: 0}, 200);
        }
    });

    $(".title-slider").css('display', 'block');
})();
