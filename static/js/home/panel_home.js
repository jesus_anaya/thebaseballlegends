
function remove_item(id_item, type) {
    if(confirm("Are you sure that you want to remove this element?")) {
        window.location.href = "/panel/remove/" + id_item + "/" + type + "/";
    }
}


function change_slides(obj, id_panel) {
    var num = $(obj).parent().find(".modify-text").val();
    window.location.href = "/panel/change-slides/" + id_panel + "/" + num + "/";
}

$(document).on('ready', function() {
    var no_border = {
        'border-bottom': 0,
        '-webkit-box-shadow': 'none',
        'box-shadow': 'none'
    };

    // $(".panel-container").each(function() {
    //     var img = $(this).find(".panel-img");
    //     var height = (img.parent().height() - img.height()) / 2;
    //     img.css({'margin-top': height + 'px'});
    // });

    $(".two-items-panel").last().css(no_border);
    $(".three-items-panel").last().css(no_border);
});
