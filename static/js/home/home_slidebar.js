/*
 * Tutorial Easy slider
 * Autor: Jesus Anaya
 * Fecha: 09/12/2013
 * Compártelo como quieras
 */

function slidebar(header_slider, container, slider_panel, prev, next, show_slides, offset)
{
    var slides, timer, totalWidth, totalSlides, position, slide_width;

    function sliderScroll(direction)
    {
       //Calcula la posición actual del contenedor
       position = $(header_slider).scrollLeft();

       //Se comprueba la variable direction para hacer el scroll hacia izquierda o derecha
       switch (direction) {
            case 'right': //Derecha
                if (totalWidth - position - 10 <= show_slides * slide_width){ //Si la siguiente posición se sale del contenedor, vuelve al principio.
                    $(header_slider + ':not(:animated)').animate({scrollLeft: 0}, "slow");
                } else { //Si no es el final, suma a la posición actual la anchura del slide.
                    $(header_slider + ':not(:animated)').animate({scrollLeft: position + slide_width},  "slow");
                }
                break;

            case 'left': //Izquierda
                if (position <= 0){ //Si la siguiente posición se sale del contenedor, vuelve al final.
                    $(header_slider + ':not(:animated)').animate({scrollLeft: totalWidth}, "slow");
                } else { //Si no es el final, resta a la posición actual la anchura del slide.
                    $(header_slider + ':not(:animated)').animate({scrollLeft: position - slide_width}, "slow");
                }
                break;
        }
    }

    //Función que crea el temporizador
    function initTimer(){
        timer = setInterval(function(){sliderScroll('right');}, 5000);
    }

    slides = $(header_slider + " > " + container + " > " + slider_panel);

    if (slides.length > 0) {
        slide_width = slides.first().outerWidth(true);
        totalWidth = (slides.length * slide_width);
        totalSlides = slides.length;

        $(header_slider + " " + container).css('width', totalWidth + 'px');
    }

    $(next).click(function()
    {
        clearInterval(timer); //Desactiva el temporizador
        sliderScroll('right'); //Mueve el scroll a la derecha
        initTimer(); //Vuelve a activar el temporizador
        return false;
    });

    //Click en el botón "prev"
    $(prev).click(function()
    {
        clearInterval(timer); //Desactiva el temporizador
        sliderScroll('left'); //Mueve el scroll a la izquierda
        initTimer(); //Vuelve a activar el temporizador
        return false;
    });

    //Inicia el temporizador
    initTimer();
}

$(document).ready(function() {
    new slidebar('#header-slider', '.slidesContainer', '.slider-panel', '.prev-arrow', '.next-arrow', 4, 20);
    new slidebar('#elite-container', '.items-container', '.elite-content-panel', '.world-prev-arrow', '.world-next-arrow', 3, 0);
});



