var MediaFileBrowser = function(field_name, url, type, win)
{
    var cmsURL = '/admin/filebrowser/browse/?pop=2';
    cmsURL += '&type=' + type;

    tinymce.activeEditor.windowManager.open({
        title: "TBL file browser",
        file: cmsURL,
        width: 980,  // Your dimensions may differ - toy around with them!
        height: 500,
        resizable: 'yes',
        scrollbars: 'yes',
        inline: 'no',  // This parameter only has an effect if you use the inlinepopups plugin!
        close_previous: 'no'
    }, {
        setUrl: function (url) {
            win.document.getElementById(field_name).value = url;
        },
        window: win,
        input: field_name
    });
    return false;
};

tinymce.init({
    selector: "textarea",
    menubar:false,
    statusbar: false,

    width: 430,
    height: 210,

    file_browser_callback: MediaFileBrowser,

    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code",
        "insertdatetime media table contextmenu paste"
    ],

    toolbar: "insertfile undo redo | styleselect bold italic | alignleft aligncenter alignright alignjustify link",
    autosave_ask_before_unload: false,
});
