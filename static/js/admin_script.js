(function($)
{
	$(document).ready(function()
	{
		//disable youtube url for defult
		$("#id_youtube_url").attr("disabled", true);

		$("#id_is_youtube").click(function(event)
		{
			if($(this).is(":checked"))
			{
				$("#id_video").attr("disabled", true);
				$("#id_video").val("");
				$("#id_youtube_url").attr("disabled", false);
			}
			else
			{
				$("#id_video").attr("disabled", false);
				$("#id_youtube_url").val("");
				$("#id_youtube_url").attr("disabled", true);
			}
		});
	});
})(grp.jQuery);