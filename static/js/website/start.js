(function()
{
    var background_image = $("meta[name='background-image']").attr('content');

    $('body').css('background', '#000 url("' + background_image + '") no-repeat top center');
})();