function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

(function($) {
    var values = getUrlVars();

    $("select#select-year").find("option").each(function() {
        if(values.year) {
            if($(this).val() === values.year) {
                $(this).attr('selected', true);
            }
        }
    });

    $("select#select-month").find("option").each(function() {
        if(values.month) {
            if($(this).val() === values.month) {
                $(this).attr('selected', true);
            }
        }
    });

    $("#filter-go").on("click", function(){
        var url = "/gallery/?";

        if($("select#select-year").val() !== "") {
            url += "year=" + $("select#select-year").val() + "&";
        }
        if($("select#select-month").val() !== "") {
            url += "month=" + $("select#select-month").val();
        }
        window.location.href = url;
    });
})(jQuery);