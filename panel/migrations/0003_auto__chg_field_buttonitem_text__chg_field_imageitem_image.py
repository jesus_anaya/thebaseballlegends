# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'ButtonItem.text'
        db.alter_column(u'panel_buttonitem', 'text', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

        # Changing field 'ImageItem.image'
        db.alter_column(u'panel_imageitem', 'image', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True))

    def backwards(self, orm):

        # Changing field 'ButtonItem.text'
        db.alter_column(u'panel_buttonitem', 'text', self.gf('django.db.models.fields.CharField')(default='', max_length=255))

        # Changing field 'ImageItem.image'
        db.alter_column(u'panel_imageitem', 'image', self.gf('stdimage.fields.StdImageField')(default='', max_length=100, upload_to='panel/images/', size={'width': 150, 'force': None, 'height': 110}))

    models = {
        u'panel.buttonitem': {
            'Meta': {'object_name': 'ButtonItem'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item_type': ('django.db.models.fields.CharField', [], {'default': "'button'", 'max_length': '20', 'blank': 'True'}),
            'panel': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['panel.Panel']"}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        },
        u'panel.imageitem': {
            'Meta': {'object_name': 'ImageItem'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'item_type': ('django.db.models.fields.CharField', [], {'default': "'image'", 'max_length': '20', 'blank': 'True'}),
            'panel': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['panel.Panel']"}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        },
        u'panel.panel': {
            'Meta': {'object_name': 'Panel'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'num_items': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'token': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['panel']