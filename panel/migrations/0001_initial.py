# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Panel'
        db.create_table(u'panel_panel', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('token', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('num_items', self.gf('django.db.models.fields.IntegerField')(default=1)),
        ))
        db.send_create_signal(u'panel', ['Panel'])

        # Adding model 'ButtonItem'
        db.create_table(u'panel_buttonitem', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('panel', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['panel.Panel'])),
            ('url', self.gf('django.db.models.fields.URLField')(max_length=200)),
            ('text', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'panel', ['ButtonItem'])

        # Adding model 'ImageItem'
        db.create_table(u'panel_imageitem', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('panel', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['panel.Panel'])),
            ('url', self.gf('django.db.models.fields.URLField')(max_length=200)),
            ('image', self.gf('stdimage.fields.StdImageField')(max_length=100, upload_to='panel/images/', size={'width': 150, 'force': None, 'height': 110})),
        ))
        db.send_create_signal(u'panel', ['ImageItem'])


    def backwards(self, orm):
        # Deleting model 'Panel'
        db.delete_table(u'panel_panel')

        # Deleting model 'ButtonItem'
        db.delete_table(u'panel_buttonitem')

        # Deleting model 'ImageItem'
        db.delete_table(u'panel_imageitem')


    models = {
        u'panel.buttonitem': {
            'Meta': {'object_name': 'ButtonItem'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'panel': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['panel.Panel']"}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        },
        u'panel.imageitem': {
            'Meta': {'object_name': 'ImageItem'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('stdimage.fields.StdImageField', [], {'max_length': '100', 'upload_to': "'panel/images/'", 'size': "{'width': 150, 'force': None, 'height': 110}"}),
            'panel': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['panel.Panel']"}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        },
        u'panel.panel': {
            'Meta': {'object_name': 'Panel'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'num_items': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'token': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['panel']