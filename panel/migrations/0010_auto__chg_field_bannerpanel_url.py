# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'BannerPanel.url'
        db.alter_column(u'panel_bannerpanel', 'url', self.gf('core.fields.MultiURLField')(max_length=200))

    def backwards(self, orm):

        # Changing field 'BannerPanel.url'
        db.alter_column(u'panel_bannerpanel', 'url', self.gf('django.db.models.fields.URLField')(max_length=200))

    models = {
        u'panel.bannerpanel': {
            'Meta': {'object_name': 'BannerPanel'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('stdimage.fields.StdImageField', [], {'max_length': '100', 'upload_to': "'blog/originals/'", 'size': "{'width': 600, 'force': None, 'height': 400}"}),
            'token': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'url': ('core.fields.MultiURLField', [], {'default': "''", 'max_length': '200'})
        },
        u'panel.buttonitem': {
            'Meta': {'object_name': 'ButtonItem'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item_type': ('django.db.models.fields.CharField', [], {'default': "'button'", 'max_length': '20', 'blank': 'True'}),
            'panel': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['panel.Panel']"}),
            'slide': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'url': ('django.db.models.fields.URLField', [], {'default': "''", 'max_length': '200', 'blank': 'True'})
        },
        u'panel.imageitem': {
            'Meta': {'object_name': 'ImageItem'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'item_type': ('django.db.models.fields.CharField', [], {'default': "'image'", 'max_length': '20', 'blank': 'True'}),
            'panel': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['panel.Panel']"}),
            'slide': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'url': ('django.db.models.fields.URLField', [], {'default': "''", 'max_length': '200', 'blank': 'True'})
        },
        u'panel.panel': {
            'Meta': {'object_name': 'Panel'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'no_margin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'num_items': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'token': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'use_extra_width': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['panel']