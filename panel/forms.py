from django import forms


class BannerPanelForm(forms.ModelForm):
    internal_more_info = forms.BooleanField(widget=forms.HiddenInput)
    internal_whos_comming = forms.BooleanField(widget=forms.HiddenInput)
