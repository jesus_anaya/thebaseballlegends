from django.contrib import admin
from django.utils.safestring import mark_safe
from .models import Panel, ImageItem, ButtonItem, BannerPanel
from .forms import BannerPanelForm


class ImagesInline(admin.TabularInline):
    model = ImageItem
    fields = ('view_image', 'image', 'url')
    readonly_fields = ('view_image',)
    extra = 0

    def view_image(self, obj):
        return mark_safe("<img src='/media/%s' width='70'>" % obj.image)
    view_image.short_description = ""


class ButtonInline(admin.TabularInline):
    model = ButtonItem
    fields = ('text', 'url')
    extra = 1


class PanelAdmin(admin.ModelAdmin):
    list_display = ('title', 'token', 'num_items')
    list_editable = ('num_items',)
    inlines = (ImagesInline, ButtonInline)


class BannerPanelAdmin(admin.ModelAdmin):
    list_display = ('title', 'view_image', 'token')

    def view_image(self, obj):
        return mark_safe("<img src='/media/%s' width='80'>" % obj.image)
    view_image.short_description = ""


admin.site.register(Panel, PanelAdmin)
admin.site.register(BannerPanel, BannerPanelAdmin)
