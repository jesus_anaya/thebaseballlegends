from django.db import models
from stdimage import StdImageField
from core.fields import MultiURLField
#from core.managers import MultiURLManager


class BannerPanel(models.Model):
    title = models.CharField(max_length=255, default="")
    image = StdImageField(upload_to="blog/originals/", size=(600, 400))
    more_info = MultiURLField(default="")
    whos_comming = MultiURLField(default="")
    token = models.CharField(max_length=255, default="")


class Panel(models.Model):
    title = models.CharField(max_length=255)
    token = models.CharField(max_length=255)
    num_items = models.IntegerField(default=1)
    no_margin = models.BooleanField(default=False)
    use_extra_width = models.IntegerField(default=0)

    def extra_width(self):
        if self.use_extra_width > 0:
            return "style=width:%dpx;max-width:none;" % self.use_extra_width
        return ""

    def use_no_margin(self):
        if self.no_margin:
            return "style=margin-right:0"
        return ""

    def __unicode__(self):
        return self.title


class Item(models.Model):
    panel = models.ForeignKey(Panel)
    url = models.URLField(default="", blank=True)
    slide = models.IntegerField(default=1)

    class Meta:
        abstract = True


class ButtonItem(Item):
    text = models.CharField(max_length=255, blank=True, null=True)
    item_type = models.CharField(max_length=20, blank=True, default="button")


class ImageItem(Item):
    image = models.ImageField(upload_to="panel/images/", blank=True, null=True)
    item_type = models.CharField(max_length=20, blank=True, default="image")
