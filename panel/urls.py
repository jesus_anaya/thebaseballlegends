from django.conf.urls import patterns, url

urlpatterns = patterns('',
    url(r"^add/(?P<id>.*)/(?P<item_type>.*)/(?P<slide>.*)/$", "panel.views.add", name="panel_add"),
    url(r"^remove/(?P<id>.*)/(?P<item_type>.*)/$", "panel.views.remove", name="panel_remove"),
    url(r"^change-slides/(?P<id_panel>.*)/(?P<num>.*)/$", "panel.views.change_slides", name="change_slides"),
)