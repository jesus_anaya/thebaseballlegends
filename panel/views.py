from django.shortcuts import redirect
from .models import Panel, ButtonItem, ImageItem


def change_slides(request, id_panel, num):
    try:
        num = int(num) if int(num) <= 3 else 3
        num = num if num > 0 else 1

        panel = Panel.objects.get(id=int(id_panel))
        panel.num_items = num
        panel.save()
    except:
        pass
    return redirect('/')


def add(request, id, item_type, slide):
    try:
        if item_type == "button":
            item = ButtonItem(panel_id=id)
        elif item_type == "image":
            item = ImageItem(panel_id=id)
        item.slide = int(slide)
        item.save()
    except:
        pass
    return redirect('/')


def remove(request, id, item_type):
    try:
        if item_type == "button":
            item = ButtonItem.objects.get(id=id)
        elif item_type == "image":
            item = ImageItem.objects.get(id=id)
        item.delete()
    except:
        pass
    return redirect('/')