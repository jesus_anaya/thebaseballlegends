from django import template
from panel.models import Panel, ButtonItem, ImageItem, BannerPanel
register = template.Library()

def class_name(num_items):
    if num_items == 1:
        return "single-item-panel"
    elif num_items == 2:
        return "two-items-panel"
    elif num_items == 3:
        return "three-items-panel"


@register.inclusion_tag("panel/banner_panle.html")
def render_banner_panel(token):
    return {
        'panel': BannerPanel.objects.get(token=token)
    }

@register.inclusion_tag("panel/home_panel.html", takes_context=True)
def render_panel(context, token):
    panel = Panel.objects.get(token=token)
    return {
        'panel': panel,
        'request': context['request'],
        'items': range(1, panel.num_items + 1),
        'class_name': class_name(panel.num_items),
        'num_items': panel.num_items
    }

@register.inclusion_tag("panel/button_item.html", takes_context=True)
def render_button_item(context, panel, slide):
    buttons = ButtonItem.objects.filter(panel=panel, slide=slide)
    return {
        'buttons': buttons,
        'request': context['request']
    }

@register.inclusion_tag("panel/image_item.html", takes_context=True)
def render_image_item(context, panel, slide):
    images = ImageItem.objects.filter(panel=panel, slide=slide)
    return {
        'images': images,
        'request': context['request']
    }
