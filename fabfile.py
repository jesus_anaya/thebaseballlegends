from fabric.api import env, local, run, cd, sudo
from fabric.contrib.files import upload_template
from thebaseballlegends_project.local_settings import FABRIC as fab


env.hosts = fab.get('hosts')
env.user = fab.get('user')
env.password = fab.get('password')
env.port = 2222
env.venv_home = "/home3/kundy31/django/tbl2"
env.venv_path = "%s/venv" % env.venv_home
env.pip = "%s/bin/pip" % env.venv_path
env.manage = "%s/bin/python %s/manage.py" % (env.venv_path, env.venv_home)


# Templates definition
templates = {
    "local_settings": {
        "local_path": "deploy/local_settings.py",
        "remote_path": "%s/config/local_settings.py" % env.venv_home,
    }
}

def upload_templates():
    for name in templates:
        template = templates.get(name);
        local_path = template.get('local_path')
        remote_path = template.get('remote_path')

        # Upload template to host
        if local_path and remote_path:
            upload_template(local_path, remote_path, env, use_sudo=False, backup=False)


def restart_services():
    for name in templates:
        template = templates.get(name);
        reload_command = template.get('reload_command')

        # Seload Server
        if reload_command:
            run(reload_command)


def init_ssh():
    home_ssh = local("echo $HOME/.ssh/id_rsa", capture=True)
    env.key_filename = home_ssh


def deploy():
    init_ssh()
    upload_templates()

    with cd(env.venv_home):
        run("git pull origin master")
        run("%s install -r requirements/hostgator.txt" % env.pip)
        run("%s syncdb" % env.manage)
        run("%s migrate --all" % env.manage)
        run("%s collectstatic --noinput" % env.manage)

    restart_services()


def migrate():
    init_ssh()
    with cd(env.venv_home):
        run("git pull origin master")
        run("%s mtbl" % env.manage)


def kill():
    init_ssh()
    with cd(env.venv_home):
        run("kill -9 `ps -ef | grep \"python\" | awk '{print $2}'`")



