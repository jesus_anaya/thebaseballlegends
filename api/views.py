from django.http import HttpResponse
from django.forms import ValidationError
from django.utils import simplejson as json
from gallery.forms import PhotoForm


def add_photo(request):
    if request.method == "POST":
        photo = PhotoForm(request.POST, request.FILES)
        if photo.is_valid():
            photo.save()
            return HttpResponse(json.dumps({'success': True}))
        else:
            raise ValidationError(photo.errors)
