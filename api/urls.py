from django.conf.urls import patterns, url
from .views import add_photo


urlpatterns = patterns('',
    url(r'^add_photo/$', add_photo, name='add_photo'),
)
