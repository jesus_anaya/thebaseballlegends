"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from django.core.urlresolvers import reverse
from .models import Team
import factory


class TeamFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = Team


class SimpleTest(TestCase):
    def test_factory_models(self):
        """
        """

        team = TeamFactory.create()
        self.assertTrue(team.id is not None)

    def test_views(self):
        response = self.client.get(reverse("register"))
        self.assertEquals(response.status_code, 200)