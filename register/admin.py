from django.contrib import admin
from admin_exporter.actions import export_as_csv_action
from .models import TeamDivision, AgeGroup, Team


class TeamAdmin(admin.ModelAdmin):
    pass


class TeamDivisionAdmin(admin.ModelAdmin):
    pass


class AgeGroupAdmin(admin.ModelAdmin):
    pass


admin.site.register(Team, TeamAdmin)
admin.site.register(TeamDivision, TeamDivisionAdmin)
admin.site.register(AgeGroup, AgeGroupAdmin)

# Add action
admin.site.add_action(export_as_csv_action)
