# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'TeamDivision'
        db.create_table(u'register_teamdivision', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=30)),
        ))
        db.send_create_signal(u'register', ['TeamDivision'])

        # Adding model 'AgeGroup'
        db.create_table(u'register_agegroup', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=30)),
        ))
        db.send_create_signal(u'register', ['AgeGroup'])

        # Adding model 'Team'
        db.create_table(u'register_team', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('manager', self.gf('django.db.models.fields.CharField')(max_length=80)),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=60)),
            ('state', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('zip_code', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('home_phone', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('cell_phone', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('email', self.gf('django.db.models.fields.CharField')(max_length=80)),
            ('team_division', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['register.TeamDivision'])),
            ('age_group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['register.AgeGroup'])),
            ('tournament', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('registered', self.gf('django.db.models.fields.DateField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'register', ['Team'])


    def backwards(self, orm):
        # Deleting model 'TeamDivision'
        db.delete_table(u'register_teamdivision')

        # Deleting model 'AgeGroup'
        db.delete_table(u'register_agegroup')

        # Deleting model 'Team'
        db.delete_table(u'register_team')


    models = {
        u'register.agegroup': {
            'Meta': {'object_name': 'AgeGroup'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'register.team': {
            'Meta': {'object_name': 'Team'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'age_group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['register.AgeGroup']"}),
            'cell_phone': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            'home_phone': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'manager': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'registered': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'team_division': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['register.TeamDivision']"}),
            'tournament': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'zip_code': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        },
        u'register.teamdivision': {
            'Meta': {'object_name': 'TeamDivision'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        }
    }

    complete_apps = ['register']