# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Team.team_division'
        db.alter_column(u'register_team', 'team_division_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['register.TeamDivision'], null=True))

        # Changing field 'Team.tournament'
        db.alter_column(u'register_team', 'tournament', self.gf('django.db.models.fields.CharField')(max_length=50, null=True))

        # Changing field 'Team.state'
        db.alter_column(u'register_team', 'state', self.gf('django.db.models.fields.CharField')(max_length=30, null=True))

        # Changing field 'Team.age_group'
        db.alter_column(u'register_team', 'age_group_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['register.AgeGroup'], null=True))

    def backwards(self, orm):

        # Changing field 'Team.team_division'
        db.alter_column(u'register_team', 'team_division_id', self.gf('django.db.models.fields.related.ForeignKey')(default=None, to=orm['register.TeamDivision']))

        # Changing field 'Team.tournament'
        db.alter_column(u'register_team', 'tournament', self.gf('django.db.models.fields.CharField')(default=None, max_length=50))

        # Changing field 'Team.state'
        db.alter_column(u'register_team', 'state', self.gf('django.db.models.fields.CharField')(default=None, max_length=30))

        # Changing field 'Team.age_group'
        db.alter_column(u'register_team', 'age_group_id', self.gf('django.db.models.fields.related.ForeignKey')(default=None, to=orm['register.AgeGroup']))

    models = {
        u'register.agegroup': {
            'Meta': {'object_name': 'AgeGroup'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'register.team': {
            'Meta': {'object_name': 'Team'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'age_group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['register.AgeGroup']", 'null': 'True', 'blank': 'True'}),
            'cell_phone': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            'home_phone': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'manager': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'registered': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'team_division': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['register.TeamDivision']", 'null': 'True', 'blank': 'True'}),
            'tournament': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'zip_code': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        },
        u'register.teamdivision': {
            'Meta': {'object_name': 'TeamDivision'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        }
    }

    complete_apps = ['register']