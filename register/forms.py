from django import forms
from .models import Team
from captcha.fields import ReCaptchaField


class TeamForm(forms.ModelForm):
    class Meta:
        model = Team

    captcha = ReCaptchaField(label="")

    def __init__(self, *args, **kwargs):
        super(TeamForm, self).__init__(*args, **kwargs)

        self.fields["state"].required = False
        self.fields["team_division"].required = False
        self.fields["age_group"].required = False
        self.fields["tournament"].required = False
