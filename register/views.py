from django.views.generic import CreateView
from django.core.urlresolvers import reverse_lazy
from website.emails import send_email
from .forms import TeamForm
from .models import Team


class RegiterView(CreateView):
    template_name = "register/request_a_spot.html"
    form_class = TeamForm
    success_url = reverse_lazy("home")
    model = Team

    def form_valid(self, form):
        send_email("TBL Request a spot", "emails/new_register.html",{
                'team': form.cleaned_data
            })
        return super(RegiterView, self).form_valid(form)
