from django.db import models
from captcha.fields import ReCaptchaField


STATES_LIST = (
    "Alabama",
    "Alaska",
    "Arizona",
    "Arkansas",
    "California",
    "Colorado",
    "Connecticut",
    "Delaware",
    "Florida",
    "Georgia",
    "Idaho",
    "Illinois",
    "Indiana",
    "Iowa",
    "Kansas",
    "Kentucky",
    "Louisiana",
    "Maine",
    "Maryland",
    "Massachusetts",
    "Hawaii",
    "Michigan",
    "Minnesota",
    "Mississippi",
    "Missouri",
    "Montana",
    "Nebraska",
    "Nevada",
    "New Hampshire",
    "New Jersey",
    "New Mexico",
    "New York",
    "North Carolina",
    "North Dakota",
    "Ohio",
    "Oklahoma",
    "Oregon",
    "Pennsylvania",
    "Rhode Island",
    "South Carolina",
    "South Dakota",
    "Tennessee",
    "Texas",
    "Utah",
    "Vermont",
    "Virginia",
    "Washington",
    "West Virginia",
    "Wisconsin",
    "Wyoming",
)


class TeamDivision(models.Model):
    name = models.CharField(max_length=30)

    def __unicode__(self):
        return self.name


class AgeGroup(models.Model):
    name = models.CharField(max_length=30)

    def __unicode__(self):
        return self.name


class Team(models.Model):
    name = models.CharField(max_length=255)
    manager = models.CharField(max_length=80)
    address = models.CharField(max_length=200)
    city = models.CharField(max_length=60)
    state = models.CharField(max_length=30, choices=zip(STATES_LIST, STATES_LIST),
                                                            blank=True, null=True)
    zip_code = models.CharField(max_length=10)
    home_phone = models.CharField(max_length=20)
    cell_phone = models.CharField(max_length=20)
    email = models.CharField(max_length=80)
    team_division = models.ForeignKey(TeamDivision, blank=True, null=True)
    age_group = models.ForeignKey(AgeGroup, blank=True, null=True)
    tournament = models.CharField(max_length=50, blank=True, null=True)
    registered = models.DateField(auto_now_add=True)

    def __unicode__(self):
        return u"%s - %s / %s" % (self.name, self.age_group, self.team_division)
