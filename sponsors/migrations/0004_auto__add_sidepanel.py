# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'SidePanel'
        db.create_table(u'sponsors_sidepanel', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('url', self.gf('django.db.models.fields.URLField')(max_length=200)),
            ('enable', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('image_left', self.gf('stdimage.fields.StdImageField')(max_length=100, upload_to='sponsors/sidepanel/', thumbnail_size={'width': 160, 'force': True, 'height': 550}, size={'width': 160, 'force': None, 'height': 550})),
            ('image_right', self.gf('stdimage.fields.StdImageField')(max_length=100, upload_to='sponsors/sidepanel/', thumbnail_size={'width': 160, 'force': True, 'height': 550}, size={'width': 160, 'force': None, 'height': 550})),
            ('start_date', self.gf('django.db.models.fields.DateField')()),
            ('end_date', self.gf('django.db.models.fields.DateField')()),
        ))
        db.send_create_signal(u'sponsors', ['SidePanel'])


    def backwards(self, orm):
        # Deleting model 'SidePanel'
        db.delete_table(u'sponsors_sidepanel')


    models = {
        u'sponsors.sidepanel': {
            'Meta': {'object_name': 'SidePanel'},
            'enable': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'end_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image_left': ('stdimage.fields.StdImageField', [], {'max_length': '100', 'upload_to': "'sponsors/sidepanel/'", 'thumbnail_size': "{'width': 160, 'force': True, 'height': 550}", 'size': "{'width': 160, 'force': None, 'height': 550}"}),
            'image_right': ('stdimage.fields.StdImageField', [], {'max_length': '100', 'upload_to': "'sponsors/sidepanel/'", 'thumbnail_size': "{'width': 160, 'force': True, 'height': 550}", 'size': "{'width': 160, 'force': None, 'height': 550}"}),
            'start_date': ('django.db.models.fields.DateField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        },
        u'sponsors.sponsor': {
            'Meta': {'object_name': 'Sponsor'},
            'enable': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'end_date': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('stdimage.fields.StdImageField', [], {'max_length': '100', 'upload_to': "'sponsors/'", 'thumbnail_size': "{'width': 100, 'force': True, 'height': 70}", 'size': "{'width': 1024, 'force': None, 'height': 768}"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'size': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'start_date': ('django.db.models.fields.DateTimeField', [], {}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['sponsors']