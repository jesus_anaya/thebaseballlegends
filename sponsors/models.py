from django.db import models
from stdimage import StdImageField
from .managers import SponsorManager, IMAGE_SIZES


class Sponsor(models.Model):
    name = models.CharField(max_length=255)
    image = StdImageField(upload_to="sponsors/", size=(1024, 768),
                        thumbnail_size=(100, 70, True))
    size = models.IntegerField(default=0, choices=IMAGE_SIZES)
    url = models.URLField()
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    enable = models.BooleanField(default=True)

    # Custom Manager
    objects = SponsorManager()

    def width(self):
        return int(IMAGE_SIZES[int(self.size)][1].split('x')[0])

    def height(self):
        return int(IMAGE_SIZES[int(self.size)][1].split('x')[1])

    def __unicode__(self):
        return u"%s - %s" % (self.name, IMAGE_SIZES[int(self.size)][1])


class SidePanel(models.Model):
    title = models.CharField(max_length=255)
    url = models.URLField()
    enable = models.BooleanField(default=False)

    image_left = StdImageField(upload_to="sponsors/sidepanel/", size=(160, 550),
                                        thumbnail_size=(160, 550, True))

    image_right = StdImageField(upload_to="sponsors/sidepanel/", size=(160, 550),
                                        thumbnail_size=(160, 550, True))

    start_date = models.DateField()
    end_date = models.DateField()

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return self.url

