"""
"""

from django.test import TestCase
from .models import Sponsor, IMAGE_SIZES
from datetime import datetime
import factory


class SponsorFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = Sponsor

    name = "Sponsor 1"
    image = factory.django.ImageField()
    size = IMAGE_SIZES[0][0]
    start_date = datetime.now()
    end_date = datetime.now()


class SimpleTest(TestCase):
    def test_model_sponsor_validation(self):
        """
        """
        sponsor = SponsorFactory.create()
        self.assertTrue(sponsor.id is not None)
