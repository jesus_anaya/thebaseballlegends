from django.contrib import admin
from django.utils.html import mark_safe as ms
from .models import Sponsor, SidePanel


class SponsorAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('name', 'image', 'size', 'enable')
        }),
        ('Sponsor URL', {
            'fields': ('url',)
        }),
        ('Life time', {
            'fields': ('start_date', 'end_date')
        })
    )

    list_display = ('name', 'thumbnail', 'size', 'start_date', 'end_date')
    list_editable = ('start_date', 'end_date')

    def thumbnail(self, obj):
        template = "<img height='70' width='100' src='{0}'>"
        return ms(template.format(obj.image.thumbnail.url()))


class SidePanelAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('title', 'url', 'enable')
        }),
        ('Images', {
            'fields': ('image_left', 'image_right')
        }),
        ('Dates', {
            'fields': ('start_date', 'end_date')
        })
    )

    list_display = ('title', 'enable', 'start_date', 'end_date')
    list_editable = ('enable', 'start_date', 'end_date')


admin.site.register(Sponsor, SponsorAdmin)
admin.site.register(SidePanel, SidePanelAdmin)

