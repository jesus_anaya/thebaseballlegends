import random
from django import template
from datetime import date
from sponsors.models import Sponsor, SidePanel
register = template.Library()


@register.inclusion_tag("sponsors/render_sponsor.html")
def render_sponsor(size):
    return {'sponsor': Sponsor.objects.get_random(size)}


@register.inclusion_tag("sponsors/sidepanels.html")
def render_sidepanels():
    sidepanels = SidePanel.objects.filter(enable=True,
        start_date__lte=date.today(), end_date__gte=date.today())

    sidepanels_count = sidepanels.count()
    if sidepanels_count > 0:
        sidepanel = sidepanels[random.randrange(sidepanels_count)]
    else:
        sidepanel = None

    return {'sidepanel': sidepanel}
