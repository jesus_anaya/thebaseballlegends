from django.db.models import Manager
from random import randint
from datetime import datetime


IMAGE_SIZES = (
    (0, "300x225"),
    (1, "300x250"),
    (2, "300x125"),
    (3, "208x230"),
    (4, "150x406"),
)


def get_size(size):
    for item in IMAGE_SIZES:
        if item[1] == size:
            return item[0]
    return None


class SponsorManager(Manager):
    """
    Sponsors filter validators
    """
    def get_random(self, size):
        now = datetime.now()
        queryset = self.filter(size=get_size(size), enable=True)
        queryset = queryset.filter(start_date__lte=now, end_date__gte=now)

        if queryset.exists():
            return queryset[randint(0, queryset.count() - 1)]
        return None
