from django import template
from widgets import site
register = template.Library()


@register.simple_tag(takes_context=True)
def render_widget(context, token, slot):
    widget = site.get_widget(token, slot)

    if widget is not None:
        return widget.render(context)
    return ""
