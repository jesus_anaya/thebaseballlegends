from django.db import models


class Board(models.Model):
    name = models.CharField(max_length=200)
    width = models.IntegerField(default=100)
    height = models.IntegerField(default=100)
    enable = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.name


class Widget(models.Model):
    #board = models.ForeignKey(Board)
    name = models.CharField(max_length=200)
    slot = models.CharField(max_length=100, verbose_name=u'Widgetslot', unique=True)
    token = models.CharField(max_length=100, verbose_name=u'Widget class token', unique=True)
    position_x = models.IntegerField(default=0)
    position_y = models.IntegerField(default=0)
    enable = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.name
