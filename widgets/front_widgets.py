from .widget import WidgetBase
from widgets import site
from gallery.models import Album


class GalleryWidget(WidgetBase):
    name = "Regular Widget"
    token = "regular-widget"
    template_name = "gallery_widget.html"

    def get_extra_context(self, context):
        context['albums'] = Album.objects.all()[:3]
        return context


site.register(GalleryWidget)