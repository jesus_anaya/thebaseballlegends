from django.conf import settings as django_settings
from django.utils.importlib import import_module
from django.utils.module_loading import module_has_submodule
from .widgets_manager import WidgetManager

# Define widget manager
site = WidgetManager()


def initial_modules():
    for app in django_settings.INSTALLED_APPS:
        if not app.startswith('django.'):
            module = import_module(app)
            try:
                import_module("%s.front_widgets" % app)
            except ImportError:
                if module_has_submodule(module, "front_widgets"):
                    raise

initial_modules()
