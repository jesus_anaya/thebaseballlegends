from operator import attrgetter
from .models import Widget


class WidgetManager(object):
    widgets_list = []

    def register(self, widget):
        self.widgets_list.append(widget())

    def get_widget(self, token, slot):
        try:
            index = map(attrgetter('token'), self.widgets_list).index(token)
            widget = self.widgets_list[index]
            try:
                widget.instance = Widget.objects.get(token=token, slot=slot)
                return widget
            except (Widget.DoesNotExist, AttributeError):
                print ("Not exists an object for widget token")
                return None
        except ValueError:
            print("The widget token does not exist")
            return None
