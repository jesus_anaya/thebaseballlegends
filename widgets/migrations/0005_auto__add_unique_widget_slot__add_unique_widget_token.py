# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding unique constraint on 'Widget', fields ['slot']
        db.create_unique(u'widgets_widget', ['slot'])

        # Adding unique constraint on 'Widget', fields ['token']
        db.create_unique(u'widgets_widget', ['token'])


    def backwards(self, orm):
        # Removing unique constraint on 'Widget', fields ['token']
        db.delete_unique(u'widgets_widget', ['token'])

        # Removing unique constraint on 'Widget', fields ['slot']
        db.delete_unique(u'widgets_widget', ['slot'])


    models = {
        u'widgets.board': {
            'Meta': {'object_name': 'Board'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'enable': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'height': ('django.db.models.fields.IntegerField', [], {'default': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'width': ('django.db.models.fields.IntegerField', [], {'default': '100'})
        },
        u'widgets.widget': {
            'Meta': {'object_name': 'Widget'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'enable': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'position_x': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'position_y': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'slot': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'token': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'})
        }
    }

    complete_apps = ['widgets']