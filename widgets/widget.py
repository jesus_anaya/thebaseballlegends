from django.template.loader import render_to_string


class WidgetBase(object):
    # default name for widget
    name = "Non named"

    instance = None

    # non token default
    token = None

    # set template name, non default name
    template_name = ""

    # set template root path name default 'widgets'
    template_root_path = "widgets"

    context = {}

    def __init__(self, *args, **kwargs):
        pass

    def render_template(self):
        return render_to_string("%s/%s" % (self.template_root_path,
                                                self.template_name),
                                                self.get_context())

    def render(self, context):
        self.context = context
        return self.render_template()

    def get_extra_context(self, context):
        return context

    def get_context(self):
        self.context['widget'] = self.instance
        return self.get_extra_context(self.context)

    def name(self):
        return u"%s" % (self.token)
