# -*- coding: utf-8 -*-
from django.db import models
from stdimage import StdImageField


class Article(models.Model):
    class Meta:
        ordering = ('-created',)

    name = models.CharField(max_length=100)
    price = models.FloatField()
    image = StdImageField(upload_to='shop/', size=(200, 165))
    url_buy = models.URLField(blank=True, null=True)
    url_info = models.URLField(blank=True, null=True)
    in_front = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return u"%s" % self.name
