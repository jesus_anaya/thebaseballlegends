from django.conf.urls import patterns, url
from .views import Shop

urlpatterns = patterns('',
    url(r"^$", Shop.as_view(), name="shop"),
)
