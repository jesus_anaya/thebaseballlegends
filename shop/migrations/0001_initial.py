# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Article'
        db.create_table(u'shop_article', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('price', self.gf('django.db.models.fields.FloatField')()),
            ('image', self.gf('stdimage.fields.StdImageField')(max_length=100, upload_to='shop/', size={'width': 200, 'force': None, 'height': 165})),
            ('url_buy', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
            ('url_info', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
            ('in_front', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'shop', ['Article'])


    def backwards(self, orm):
        # Deleting model 'Article'
        db.delete_table(u'shop_article')


    models = {
        u'shop.article': {
            'Meta': {'ordering': "('-created',)", 'object_name': 'Article'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('stdimage.fields.StdImageField', [], {'max_length': '100', 'upload_to': "'shop/'", 'size': "{'width': 200, 'force': None, 'height': 165}"}),
            'in_front': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'price': ('django.db.models.fields.FloatField', [], {}),
            'url_buy': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'url_info': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['shop']