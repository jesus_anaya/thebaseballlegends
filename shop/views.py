from django.views.generic import ListView
from .models import Article

class Shop(ListView):
    template_name = "shop/shop.html"
    context_object_name = "articles"
    model = Article
