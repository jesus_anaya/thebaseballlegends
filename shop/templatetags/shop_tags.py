from django import template
from shop.models import Article
register = template.Library()

@register.inclusion_tag("shop/shop_widget.html")
def render_shop_widget():
    return {
        'articles': Article.objects.filter(in_front=True)[:3]
    }
