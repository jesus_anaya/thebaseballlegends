from django import template
from django.core.urlresolvers import reverse
from blog.models import PostCategory as PC
from website.models import Background

register = template.Library()


@register.simple_tag
def menu_post_categories_list():
    li_tag = "<li><a href='%s?category={0}'>{1}</a></li>" % reverse(
                                                    "blog_posts_list")

    return [li_tag.format(x.id, x.name) for x in PC.objects.all()]


@register.simple_tag
def get_background():
    try:
        return Background.objects.get(active=True).image.url
    except Background.DoesNotExist:
        return ""
