from django import template
from datetime import date
register = template.Library()

@register.simple_tag
def available_years():
    init_year = 2012
    end_year = date.today().year
    template = "<option value='{0}'>{1}</option>"

    return "".join([template.format(x, x) for x in range(
                                init_year, end_year + 1)])