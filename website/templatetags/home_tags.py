from django import template
from blog.models import Post
register = template.Library()


@register.inclusion_tag('home/news_slides.html')
def render_news_slides():
    """
    Render to last 6 news posts at the home in a slider panel
    """
    return {'posts': Post.objects.all()[:6]}


@register.inclusion_tag('home/headlines.html')
def render_news_headlines():
    """
    Render to last 6 news posts at the home in a slider panel
    """
    return {'posts': Post.objects.all()[:6]}
