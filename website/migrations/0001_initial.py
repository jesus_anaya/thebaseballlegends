# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Background'
        db.create_table(u'website_background', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('image', self.gf('stdimage.fields.StdImageField')(max_length=100, upload_to='backgrounds/', thumbnail_size={'width': 100, 'force': True, 'height': 120})),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'website', ['Background'])


    def backwards(self, orm):
        # Deleting model 'Background'
        db.delete_table(u'website_background')


    models = {
        u'website.background': {
            'Meta': {'object_name': 'Background'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('stdimage.fields.StdImageField', [], {'max_length': '100', 'upload_to': "'backgrounds/'", 'thumbnail_size': "{'width': 100, 'force': True, 'height': 120}"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['website']