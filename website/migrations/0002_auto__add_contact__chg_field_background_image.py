# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Contact'
        db.create_table(u'website_contact', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('age_group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['register.AgeGroup'], null=True, blank=True)),
            ('team_name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('team_division', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['register.TeamDivision'], null=True, blank=True)),
            ('state', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('phone_number', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('question1', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('question2', self.gf('django.db.models.fields.CharField')(max_length=5, null=True, blank=True)),
            ('question3', self.gf('django.db.models.fields.CharField')(max_length=5, null=True, blank=True)),
            ('question4', self.gf('django.db.models.fields.CharField')(max_length=5, null=True, blank=True)),
            ('question5', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('question6', self.gf('django.db.models.fields.CharField')(max_length=5, null=True, blank=True)),
        ))
        db.send_create_signal(u'website', ['Contact'])


        # Changing field 'Background.image'
        db.alter_column(u'website_background', 'image', self.gf('stdimage.fields.StdImageField')(max_length=100, upload_to='backgrounds/', thumbnail_size={'width': 100, 'force': True, 'height': 70}))

    def backwards(self, orm):
        # Deleting model 'Contact'
        db.delete_table(u'website_contact')


        # Changing field 'Background.image'
        db.alter_column(u'website_background', 'image', self.gf('stdimage.fields.StdImageField')(max_length=100, upload_to='backgrounds/', thumbnail_size={'width': 100, 'force': True, 'height': 120}))

    models = {
        u'register.agegroup': {
            'Meta': {'object_name': 'AgeGroup'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'register.teamdivision': {
            'Meta': {'object_name': 'TeamDivision'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'website.background': {
            'Meta': {'object_name': 'Background'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('stdimage.fields.StdImageField', [], {'max_length': '100', 'upload_to': "'backgrounds/'", 'thumbnail_size': "{'width': 100, 'force': True, 'height': 70}"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'website.contact': {
            'Meta': {'object_name': 'Contact'},
            'age_group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['register.AgeGroup']", 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'phone_number': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'question1': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'question2': ('django.db.models.fields.CharField', [], {'max_length': '5', 'null': 'True', 'blank': 'True'}),
            'question3': ('django.db.models.fields.CharField', [], {'max_length': '5', 'null': 'True', 'blank': 'True'}),
            'question4': ('django.db.models.fields.CharField', [], {'max_length': '5', 'null': 'True', 'blank': 'True'}),
            'question5': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'question6': ('django.db.models.fields.CharField', [], {'max_length': '5', 'null': 'True', 'blank': 'True'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'team_division': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['register.TeamDivision']", 'null': 'True', 'blank': 'True'}),
            'team_name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['website']