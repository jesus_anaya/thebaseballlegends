from django.views.generic import TemplateView, CreateView
from django.core.urlresolvers import reverse_lazy
from .emails import send_email
from .models import Contact
from .forms import ContactForm


class HomeView(TemplateView):
    template_name = "home.html"


class ContatView(CreateView):
    template_name = "contact.html"
    form_class = ContactForm
    success_url = reverse_lazy("home")
    model = Contact

    def form_valid(self, form):
        send_email("TBL contact information", "emails/contact.html",{
                'contact': form.cleaned_data
            })
        return super(ContatView, self).form_valid(form)
