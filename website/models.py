from django.db import models
from stdimage import StdImageField
from register.models import AgeGroup, TeamDivision, STATES_LIST

class Background(models.Model):
    name = models.CharField(max_length=200)
    image = StdImageField(upload_to="backgrounds/", thumbnail_size=(100, 70, True))
    active = models.BooleanField(default=False)

    def __unicode__(self):
        return self.name

EVENTS = (
    "",
)

YESNO = (
    ("Yes", "Yes"),
    ("No", "No")
)

class Contact(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    age_group = models.ForeignKey(AgeGroup, blank=True, null=True)
    team_name = models.CharField(max_length=255,
            verbose_name='Team Name / Organization Name:')
    team_division = models.ForeignKey(TeamDivision, blank=True, null=True)
    state = models.CharField(max_length=50, choices=zip(STATES_LIST, STATES_LIST))
    email = models.EmailField()
    phone_number = models.CharField(max_length=30)

    question1 = models.CharField(max_length=100, choices=zip(EVENTS, EVENTS),
            blank=True, null=True,
            verbose_name='Will you like more information on the following event?:')

    question2 = models.CharField(max_length=5, choices=YESNO,
        blank=True, null=True,
        verbose_name='Interested in receiving a monthly TBL Newsletter?:')

    question3 = models.CharField(max_length=5, choices=YESNO,
        blank=True, null=True,
        verbose_name='Interested in running TBL Tournaments?')

    question4 = models.CharField(max_length=5, choices=YESNO,
        blank=True, null=True,
        verbose_name='Interested in becoming a State Director?')

    question5 = models.TextField(blank=True, null=True,
        verbose_name='If you answered yes to the two questions above, provide us with a brief '
                'explanation of your past history?')

    question6 = models.CharField(max_length=5, choices=YESNO,
        blank=True, null=True,
        verbose_name='Need help running a fundraising tournament?')
