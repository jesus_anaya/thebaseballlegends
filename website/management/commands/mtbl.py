# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.core.files.base import ContentFile
from django.contrib.auth import get_user_model
from django.core.files import File
import Image
from gallery.models import Photo
from urlparse import urlparse
import MySQLdb as mdb
import sys
import urllib2
import urllib
import os


User = get_user_model()

def get_image(img_url):
    img_url = "http://thebaseballlegends.com/media/" + img_url
    print "download image: ", img_url

    result = urllib.urlretrieve(img_url)
    name = os.path.basename(img_url)
    content = File(open(result[0]))

    return name, content


class Command(BaseCommand):

    def handle(self, **options):
        try:
            con = mdb.connect('localhost', 'kundy31_kundy31', 'Pp=}]v1okizi', 'kundy31_tbl',
                                                            use_unicode=True, charset='utf8')

            cur = con.cursor()
            cur.execute(r"SELECT * from baseball_photo")

            rows = cur.fetchall()

            id_album = -1;

            for row in rows:
                try:
                    photo = Photo()
                    photo.title=row[2]
                    photo.album_id=row[1]

                    if id_album != row[1]:
                        id_album = row[1]
                        photo.is_front = True
                    else:
                        photo.is_front = False

                    photo.added=row[6]

                    name, content = get_image(row[3])
                    photo.image.save(name, content)
                    photo.save()

                    print "migrated ", row[2]
                except Exception, e:
                    print row[1], e

        except mdb.Error, e:
            print "Error %d: %s" % (e.args[0],e.args[1])
            sys.exit(1)

        finally:
            if con:
                con.close()
