from django.contrib import admin
from .models import Background


class BackgroundAdmin(admin.ModelAdmin):
    list_display = ('name', 'active')
    list_editable = ('active',)


admin.site.register(Background, BackgroundAdmin)
