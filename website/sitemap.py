from django.contrib.sitemaps import Sitemap
from blog.models import Post


class TBLSitemap(Sitemap):
    changefreq = "Weekly"
    priority = 0.5

    def items(self):
        return Post.objects.all()

    def lastmod(self, obj):
        return obj.created
