from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.conf import settings


def send_email(subject, template, context):
    template_html = render_to_string(template, context)
    template_string = strip_tags(template_html)

    email = EmailMultiAlternatives(subject, template_string,
                                to=settings.RECIPERS_EMAILS)

    email.attach_alternative(template_html, "text/html")
    email.send()
