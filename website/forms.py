from django import forms
from captcha.fields import ReCaptchaField
from .models import Contact


class ContactForm(forms.ModelForm):
    class Meta:
        model = Contact

    captcha = ReCaptchaField(label="")

    def __init__(self, *args, **kwargs):
        super(ContactForm, self).__init__(*args, **kwargs)

        self.fields["age_group"].required = False
        self.fields["team_division"].required = False
        self.fields["question1"].required = False
        self.fields["question2"].required = False
        self.fields["question3"].required = False
        self.fields["question4"].required = False
        self.fields["question5"].required = False
        self.fields["question6"].required = False
