from django.db import models


class ContentManager(models.Manager):
    def enables(self):
        return self.filter(enable=True)
