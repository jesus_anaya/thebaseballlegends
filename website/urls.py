from django.conf.urls import patterns, url
from .views import HomeView, ContatView

urlpatterns = patterns('',
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^contact/$', ContatView.as_view(), name='contact'),
)