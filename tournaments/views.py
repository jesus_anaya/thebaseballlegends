from django.views.generic import DetailView, ListView
from .models import Tournament, TournamentPage


class TournamentView(DetailView):
    template_name = "tournaments/tournament_page.html"
    context_object_name = "page"
    model = TournamentPage


class TournamentsLogosView(ListView):
    template_name = "tournaments/logos_page.html"
    model = Tournament
    context_object_name = "tournaments"