from django.contrib import admin
from django.utils.safestring import mark_safe
from .models import Tournament, TournamentPage


class TournamentPageInline(admin.StackedInline):
    model = TournamentPage
    max_num = 1
    exclude = ('slug',)


class TournamentAdmin(admin.ModelAdmin):
    class Media:
        js = (
            '/static/tiny_mce/tinymce.min.js',
            '/static/js/admin/effects.js',
            '/static/js/admin/tournament.js',
            '/static/js/tinymce.js',
        )

    fieldsets = (
        ('', {
            'fields': ('name', 'enable'),
        }),
        ('Tournament type', {
            'classes': ('wp-collapse',),
            'fields' : ('tournament_type', 'view_in_front'),
        }),
        ('Main Image', {
            'classes': ('wp-collapse',),
            'fields' : ('image',),
        }),
        ('Tournament dates range', {
            'classes': ('wp-collapse',),
            'fields' : ('start_date', 'end_date'),
        }),
        ('Use external link', {
            'fields' : ('use_url', 'url', 'whos_coming_url'),
        }),
    )
    list_display = ('name', 'show_image', 'enable', 'tournament_type', 'start_date', 'end_date')
    list_editable = ('enable', 'start_date', 'end_date', 'tournament_type')
    inlines = (TournamentPageInline,)

    def show_image(self, obj):
        img = "<img src='{0}'>"
        try:
            url = obj.image.thumbnail.url()
        except AttributeError:
            url = obj.image.url()

        return mark_safe(img.format(url))

admin.site.register(Tournament, TournamentAdmin)
