"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from datetime import datetime
from .models import Tournament, TournamentPage, TOURNAMENT_TYPES
import factory
import logging


class TournamentFactory(factory.Factory):
    FACTORY_FOR = Tournament

    name = "Test tournament name"
    enable = True
    tournament_type = TOURNAMENT_TYPES[0][0]
    image = factory.django.ImageField()
    start_date = datetime.now()
    end_date = datetime.now()


class TournamentPageFactory(factory.Factory):
    FACTORY_FOR = TournamentPage

    title = "Titulo de prueba 1"
    image = factory.django.ImageField()
    body = "<h1>Ola ke Ase</h1>"
    pay_url = "http://google.com"
    scores_url = "http://google.com"
    whos_coming_url = "http://google.com"


class SimpleTest(TestCase):
    def setUp(self):
        logger = logging.getLogger('factory')
        logger.addHandler(logging.StreamHandler())
        logger.setLevel(logging.DEBUG)

    def test_tournament_model_create(self):
        """
        Test if tournament model is create correctly
        """
        tournament = TournamentFactory.create()
        self.assertTrue(tournament.id is not None)

        page = TournamentPageFactory.create(tournament=tournament)
        self.assertTrue(page.id is not None)

        self.assertEquals(page.slug, "titulo-de-prueba-1")
