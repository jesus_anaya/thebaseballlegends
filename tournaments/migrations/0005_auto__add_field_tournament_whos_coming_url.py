# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Tournament.whos_coming_url'
        db.add_column(u'tournaments_tournament', 'whos_coming_url',
                      self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Tournament.whos_coming_url'
        db.delete_column(u'tournaments_tournament', 'whos_coming_url')


    models = {
        u'tournaments.tournament': {
            'Meta': {'ordering': "['-start_date']", 'object_name': 'Tournament'},
            'enable': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'end_date': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('stdimage.fields.StdImageField', [], {'blank': 'True', 'max_length': '100', 'upload_to': "'tournaments/images/'", 'thumbnail_size': "{'width': 58, 'force': None, 'height': 94}", 'size': "{'width': 108, 'force': None, 'height': 144}"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'start_date': ('django.db.models.fields.DateTimeField', [], {}),
            'tournament_type': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'use_url': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'view_in_front': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'whos_coming_url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        u'tournaments.tournamentpage': {
            'Meta': {'object_name': 'TournamentPage'},
            'body': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('stdimage.fields.StdImageField', [], {'blank': 'True', 'max_length': '100', 'upload_to': "'tournaments/pages/'", 'size': "{'width': 600, 'force': None, 'height': 320}"}),
            'pay_url': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'scores_url': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '255', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'tournament': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['tournaments.Tournament']"}),
            'whos_coming_url': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['tournaments']