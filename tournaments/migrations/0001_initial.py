# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Tournament'
        db.create_table(u'tournaments_tournament', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('image', self.gf('stdimage.fields.StdImageField')(blank=True, max_length=100, upload_to='tournaments/images/', thumbnail_size={'width': 58, 'force': True, 'height': 94}, size={'width': 108, 'force': None, 'height': 144})),
            ('enable', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('tournament_type', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('start_date', self.gf('django.db.models.fields.DateTimeField')()),
            ('end_date', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'tournaments', ['Tournament'])

        # Adding model 'TournamentPage'
        db.create_table(u'tournaments_tournamentpage', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('tournament', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['tournaments.Tournament'])),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=255, blank=True)),
            ('image', self.gf('stdimage.fields.StdImageField')(blank=True, max_length=100, upload_to='tournaments/pages/', size={'width': 600, 'force': None, 'height': 320})),
            ('body', self.gf('django.db.models.fields.TextField')()),
            ('pay_url', self.gf('django.db.models.fields.URLField')(max_length=200)),
            ('scores_url', self.gf('django.db.models.fields.URLField')(max_length=200)),
            ('whos_coming_url', self.gf('django.db.models.fields.URLField')(max_length=200)),
        ))
        db.send_create_signal(u'tournaments', ['TournamentPage'])


    def backwards(self, orm):
        # Deleting model 'Tournament'
        db.delete_table(u'tournaments_tournament')

        # Deleting model 'TournamentPage'
        db.delete_table(u'tournaments_tournamentpage')


    models = {
        u'tournaments.tournament': {
            'Meta': {'object_name': 'Tournament'},
            'enable': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'end_date': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('stdimage.fields.StdImageField', [], {'blank': 'True', 'max_length': '100', 'upload_to': "'tournaments/images/'", 'thumbnail_size': "{'width': 58, 'force': True, 'height': 94}", 'size': "{'width': 108, 'force': None, 'height': 144}"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'start_date': ('django.db.models.fields.DateTimeField', [], {}),
            'tournament_type': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'tournaments.tournamentpage': {
            'Meta': {'object_name': 'TournamentPage'},
            'body': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('stdimage.fields.StdImageField', [], {'blank': 'True', 'max_length': '100', 'upload_to': "'tournaments/pages/'", 'size': "{'width': 600, 'force': None, 'height': 320}"}),
            'pay_url': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'scores_url': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '255', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'tournament': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['tournaments.Tournament']"}),
            'whos_coming_url': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['tournaments']