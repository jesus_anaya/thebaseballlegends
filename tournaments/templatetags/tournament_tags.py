from django import template
from tournaments.models import Tournament
from datetime import timedelta, date
register = template.Library()

REGULAR = 0
LEAGUE = 1
WORLD = 2
QUALIFIER = 3


def filter_tournaments(queryset, size, second_page, extra_list):
    if second_page:
        if queryset.count() > 17:
            tournaments = queryset[17:28]
        else:
            tournaments = []
    else:
        if size > 0:
            tournaments = queryset[:size]
        else:
            tournaments = []
    return {'tournaments': extra_list + list(tournaments)}


@register.inclusion_tag("tournaments/menu.html")
def worlds_menu(second_page=False, extra_list=[]):
    size = 17 - len(extra_list)
    queryset = Tournament.objects.enables().filter(tournament_type=WORLD)
    return filter_tournaments(queryset, size, second_page, extra_list)


@register.inclusion_tag("tournaments/menu.html")
def tournaments_menu(second_page=False, extra_list=[]):
    size = 17 - len(extra_list)
    queryset = Tournament.objects.enables().filter(name__icontains='tbl')
    return filter_tournaments(queryset, size, second_page, extra_list)


@register.inclusion_tag("tournaments/tournaments_bar.html")
def render_tournaments():
    return {
        'tournaments': Tournament.objects.enables().filter(
                                    tournament_type=LEAGUE)
    }


@register.simple_tag
def best_of_the_best_link():
    try:
        name = "TBL Best Of The Best National Championship"
        return Tournament.objects.get(name=name).get_absolute_url()
    except Tournament.DoesNotExist:
        return ""


@register.inclusion_tag("tournaments/qualifiers_home.html")
def render_qualifiers():
    queryset = Tournament.objects.enables().filter(
                            tournament_type=QUALIFIER)
    return {
        'qualifiers': queryset.filter(end_date__gte=(date.today()))[:3]
    }


@register.inclusion_tag("tournaments/worlds_bar.html")
def render_words():
    return {
        'worlds': Tournament.objects.enables().filter(
                                tournament_type=WORLD)
    }

@register.inclusion_tag("tournaments/up-coming-event.html")
def render_up_coming_event():
    t = Tournament.objects.enables()
    tournaments = t.filter(
        start_date__gte=(date.today() - timedelta(days=5)),
        end_date__lte=(date.today()))

    return {'tournaments': tournaments[:3]}
