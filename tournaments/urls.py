from django.conf.urls import patterns, url
from .views import TournamentView, TournamentsLogosView

urlpatterns = patterns('',
    url(r"^logos/$", TournamentsLogosView.as_view(), name="tournament_logo_page"),
    url(r"^(?P<slug>.*)/$", TournamentView.as_view(), name="tournament_page"),
)
