from django.db import models
from django.core.urlresolvers import reverse
from stdimage import StdImageField
from website.utils import unique_slug
from website.managers import ContentManager


TOURNAMENT_TYPES = (
    (0, "Regular"),
    (1, "League"),
    (2, "World"),
    (3, "Qualifier")
)


class TournamentPage(models.Model):
    tournament = models.ForeignKey('Tournament')
    title = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255, blank=True)
    image = StdImageField(upload_to="tournaments/pages/", size=(600, 320),
                                            thumbnail_size=(180, 135, True))
    body = models.TextField()
    pay_url = models.URLField()
    scores_url = models.URLField()
    whos_coming_url = models.URLField()

    paypal_code = models.TextField(blank=True, null=True)

    def __unicode__(self):
        return self.title

    def save(self):
        self.slug = unique_slug(self, 'title', 'slug')
        super(TournamentPage, self).save()

    def get_absolute_url(self):
        return reverse('tournament_page', args=(self.slug,))


class Tournament(models.Model):
    class Meta:
        ordering = ['start_date']

    name = models.CharField(max_length=255)
    image = StdImageField(upload_to="tournaments/images/", size=(194, 140),
                                                    thumbnail_size=(58, 94))
    enable = models.BooleanField(default=True)
    tournament_type = models.IntegerField(default=0,
                            choices=TOURNAMENT_TYPES)
    view_in_front = models.BooleanField(default=False, help_text='World tournament only')

    use_url = models.BooleanField(default=False)
    url = models.URLField(blank=True, null=True)
    whos_coming_url = models.URLField(blank=True, null=True)

    start_date = models.DateTimeField()
    end_date = models.DateTimeField()

    objects = ContentManager()

    def __unicode__(self):
        return self.name

    def get_whos_coming(self):
        if not self.use_url and self.get_page():
            return self.get_page().whos_coming_url
        else:
            return self.whos_coming_url

    def get_url(self):
        if not self.use_url and self.get_page():
            return self.get_page().get_absolute_url()
        else:
            return self.url

    def get_absolute_url(self):
        return self.get_url()

    def get_page(self):
        try:
            return TournamentPage.objects.get(tournament=self.id)
        except TournamentPage.DoesNotExist:
            return None

