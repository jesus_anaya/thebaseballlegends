from django.conf.urls import patterns, url
from .views import AlbumView, AlbumList

urlpatterns = patterns('',
    url(r"^$", AlbumList.as_view(), name="albums_list"),
    url(r"^(?P<slug>.*)/$", AlbumView.as_view(), name="album"),
)