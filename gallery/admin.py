from django.contrib import admin
from django.utils.safestring import mark_safe as ms
from .models import Album, Photo

class PhotoAdminInline(admin.TabularInline):
    model = Photo
    extra = 1


class AlbumAdmin(admin.ModelAdmin):
    class Media:
        js = (
            '/static/js/dropzone.js',
            '/static/js/admin/gallery.js',
        )
        css = {'all': ('/static/css/dropzone.css',)}

    fields = ('name',)
    list_display = ('name', 'thumbnail', 'view_on_site')
    inlines = (PhotoAdminInline,)

    def thumbnail(self, obj):
        image_url = obj.front_image()
        if image_url:
            return ms("<img src='%s' height='70'>" % obj.front_image())
        else:
            return ""

    def view_on_site(self, obj):
        return ms("<a href='%s' target='_blank'>View on site</a>"   \
                                            % obj.get_absolute_url())

admin.site.register(Album, AlbumAdmin)
