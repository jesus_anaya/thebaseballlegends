from django.views.generic import DetailView, ListView
from .models import Album

class AlbumView(DetailView):
    template_name = "gallery/album.html"
    context_object_name = "album"
    queryset = Album.objects.all()


class AlbumList(ListView):
    template_name = "gallery/albums_list.html"
    context_object_name = "albums"
    queryset = Album.objects.all()

    def get(self, request):
        year = request.GET.get("year")
        month = request.GET.get("month")

        if year:
            self.queryset = self.queryset.filter(created__year=year)

        if month:
            self.queryset = self.queryset.filter(created__month=month)

        return super(AlbumList, self).get(request)