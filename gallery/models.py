from django.db import models
from stdimage import StdImageField
from django.core.urlresolvers import reverse_lazy
from website.utils import unique_slug


class Photo(models.Model):
    class Meta:
        ordering = ('-added',)

    title = models.CharField(max_length=255)
    album = models.ForeignKey('Album')
    image = StdImageField(upload_to="gallery/photos", size=(1280, 720),
                                            thumbnail_size=(194, 140, True))
    is_front = models.BooleanField(default=False)
    added = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.title


class Album(models.Model):
    class Meta:
        ordering = ('-created',)

    name = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255, blank=True)
    created = models.DateTimeField(auto_now_add=True)

    def get_photos(self):
        return Photo.objects.filter(album=self)

    def front_image(self):
        try:
            return Photo.objects.filter(
                album=self, is_front=True)[0].image.thumbnail.url()
        except IndexError:
            return None

    def get_absolute_url(self):
        return reverse_lazy('album', args=(self.slug,))

    def save(self, *args, **kwargs):
        self.slug = unique_slug(self, 'name', 'slug')
        super(Album, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.name
