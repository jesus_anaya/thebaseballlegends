from django import template
from gallery.models import Album
register = template.Library()


@register.inclusion_tag("gallery/home_panel.html")
def render_albums():
    return {'albums': Album.objects.all()[:3]}
