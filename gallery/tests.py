"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from datetime import datetime
from .models import Album, Photo
import factory
import logging


class AlbumFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = Album

    name = "Name album 1"
    created = datetime.now()


class PhotoFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = Photo

    title = "photo 1"
    image = factory.django.ImageField()
    added = datetime.now()


class SimpleTest(TestCase):
    def setUp(self):
        logger = logging.getLogger('factory')
        logger.addHandler(logging.StreamHandler())
        logger.setLevel(logging.DEBUG)

        self.album = AlbumFactory.create()

    def test_models_creation(self):
        """
        Test - create and Album and add into a photo
        """

        self.assertTrue(self.album.id is not None)

        photo = PhotoFactory.create(album=self.album)
        self.assertTrue(photo.id is not None)

    def test_app_views(self):

        response = self.client.get('/gallery/')
        self.assertEqual(response.status_code, 200)

        response = self.client.get('/gallery/%s/' % self.album.slug)
        self.assertEqual(response.status_code, 200)

