# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Album.slug'
        db.add_column(u'gallery_album', 'slug',
                      self.gf('django.db.models.fields.SlugField')(default='', max_length=255, blank=True),
                      keep_default=False)


        # Changing field 'Photo.image'
        db.alter_column(u'gallery_photo', 'image', self.gf('stdimage.fields.StdImageField')(max_length=100, upload_to='gallery/photos', thumbnail_size={'width': 194, 'force': None, 'height': 140}, size={'width': 1280, 'force': None, 'height': 720}))

    def backwards(self, orm):
        # Deleting field 'Album.slug'
        db.delete_column(u'gallery_album', 'slug')


        # Changing field 'Photo.image'
        db.alter_column(u'gallery_photo', 'image', self.gf('stdimage.fields.StdImageField')(max_length=100, upload_to='/gallery/photos', thumbnail_size={'width': 194, 'force': None, 'height': 140}, size={'width': 1280, 'force': None, 'height': 720}))

    models = {
        u'gallery.album': {
            'Meta': {'object_name': 'Album'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '255', 'blank': 'True'})
        },
        u'gallery.photo': {
            'Meta': {'object_name': 'Photo'},
            'added': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'album': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['gallery.Album']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('stdimage.fields.StdImageField', [], {'max_length': '100', 'upload_to': "'gallery/photos'", 'thumbnail_size': "{'width': 194, 'force': None, 'height': 140}", 'size': "{'width': 1280, 'force': None, 'height': 720}"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['gallery']