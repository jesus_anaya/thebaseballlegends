from django.forms.fields import URLField
from .widgets import MultiURLWidget

class MultiURLForm(URLField):
    widget = MultiURLWidget
