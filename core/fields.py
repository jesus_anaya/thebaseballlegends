from django.db.models.fields import URLField
from .widgets import MultiURLWidget


class MultiURLField(URLField):
    def formfield(self, **kwargs):
        kwargs['widget'] = MultiURLWidget
        return super(MultiURLField, self).formfield(**kwargs)
