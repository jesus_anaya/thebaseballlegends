from django.utils.translation import ugettext_lazy as _
from django.utils.text import capfirst
from django.core.urlresolvers import reverse
from django.conf import settings

from wpadmin.utils import get_admin_site_name
from wpadmin.menu import items
from wpadmin.menu.menus import Menu

class AdminTopMenu(Menu):

    def init_with_context(self, context):

        admin_site_name = get_admin_site_name(context)

        if 'django.contrib.sites' in settings.INSTALLED_APPS:
            from django.contrib.sites.models import Site
            site_name = Site.objects.get_current().name
            site_url = 'http://' + Site.objects.get_current().domain
        else:
            site_name = capfirst(_('site'))
            site_url = '/'

        self.children += [
            items.MenuItem(
                site_name,
                url=site_url,
                css_classes=['branding', 'no-border'],
            ),
            items.MenuItem(
                capfirst(_('dashboard')),
                icon='icon-home',
                url=reverse('%s:index' % admin_site_name),
                description=capfirst(_('dashboard')),
            ),
            items.AppList(
                capfirst(_('applications')),
                icon='icon-tasks',
                exclude=('django.contrib.*',),
                check_if_user_allowed=lambda user: user.is_superuser,
            ),
            items.AppList(
                capfirst(_('administration')),
                icon='icon-cogs',
                models=('django.contrib.*',),
                check_if_user_allowed=lambda user: user.is_superuser,
            ),
            items.UserTools(
                url=reverse('%s:auth_user_change' % admin_site_name,
                            args=(context['request'].user.id,)),
                css_classes=['float-right'],
                check_if_user_allowed=lambda user: user.is_superuser,
            ),
        ]


class AdminLeftMenu(Menu):

    icons = {
        'wp-default-icon': 'icon-folder-open',
        '/admin/auth/user/': 'icon-user',
        '/admin/auth/group/': 'icon-group',
        '/admin/sites/site/': 'icon-globe',
    }

    def is_user_allowed(self, user):
        """
        Only users that are superusers are allowed to see this menu.
        """
        return user.is_superuser

    def init_with_context(self, context):

        admin_site_name = get_admin_site_name(context)

        self.children += [
            items.MenuItem(
                title='',
                children=[
                    items.MenuItem(
                        capfirst(_('dashboard')),
                        icon='icon-home',
                        url=reverse('%s:index' % admin_site_name),
                        description=capfirst(_('dashboard')),
                    )
                ]
            ),
            items.MenuItem(
                capfirst(_('Contents')),
                children=[
                    items.ModelList(
                        capfirst(_('Pages')),
                        icon='icon-book',
                        models=(
                            'pages.models.Page',
                            'pages.models.Link',
                            'pages.models.File',
                            'pages.models.Logos',
                            'pages.models.ProfilesPage',
                        ),
                    ),
                    items.ModelList(
                        capfirst(_('Content')),
                        icon='icon-book',
                        models=(
                            'blog.models.Post',
                            'tournaments.models.Tournament',
                            'sponsors.models.Sponsor',
                        ),
                    ),
                    items.ModelList(
                        capfirst(_('Home')),
                        icon='icon-book',
                        models=(
                            'popup.models.Popup',
                            'sponsors.models.SidePanel',
                            'panel.models.Panel',
                            'panel.models.BannerPanel',
                        ),
                    ),
                    items.ModelList(
                        capfirst(_('Media')),
                        icon='icon-film',
                        models=(
                            'gallery.models.Album',
                            'videos.models.VideoPost',
                        ),
                    ),
                    items.ModelList(
                        capfirst(_('Categories')),
                        models=(
                            'blog.models.PostCategory',
                        ),
                    ),
                    items.ModelList(
                        capfirst(_('Backgrounds')),
                        models=(
                            'website.models.Background',
                        ),
                    ),
                    items.ModelList(
                        capfirst(_('Shop')),
                        models=(
                            'shop.models.Article',
                        ),
                    ),
                    items.ModelList(
                        capfirst(_('authors')),
                        icon='icon-quote-right',
                        models=('authors.models.Author',),
                    ),
                ],
            ),
            items.ModelList(
                capfirst(_('users')),
                models=('django.contrib.auth.*',),
            ),
            items.ModelList(
                capfirst(_('administration')),
                models=('django.contrib.sites.*',),
            ),
        ]