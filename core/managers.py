from django.db import models
from .fields import MultiURLField


print dir(models.Manager)

class MultiURLManager(models.Manager):
    def __init__(self):
        obj = super(MultiURLManager, self).__init__()
        print dir(self.model.__class__)
        for field in self.model._meta.fields:
            if isinstance(field, MultiURLField):
                name = "internal_%s" % field.attname
                self.model.add_to_class(name, models.BooleanField(default=False))
        return obj
