# -*- coding: utf-8 -*-
from django.contrib.admin.widgets import AdminFileWidget
from django.template.loader import render_to_string
from django.utils.safestring import mark_safe
from django.contrib.sites.models import Site
from pages.models import Page
from blog.models import Post
from tournaments.models import Tournament


class MultiURLWidget(AdminFileWidget):
    input_type = 'text'

    def get_list(self, model):
        return model.objects.enables()

    def render(self, name, value, attrs=None):
        return mark_safe(render_to_string("core/multi-url.html", {
            'name': name,
            'value': value,
            'site': Site.objects.get_current(),
            'pages': self.get_list(Page),
            'blog': self.get_list(Post),
            'tournaments': self.get_list(Tournament),
        }))

    def value_from_datadict(self, data, files, name):
        return data.get(name, None)