from django import template
from pages.models import Page, Link, ProfilesPage, Logos, File

register = template.Library()


@register.inclusion_tag("pages/menu.html")
def pages_menu(section, extra_list=None):
    li_tag = "<li><a href='{0}'>{1}</a></li>"

    pages = list(Page.objects.enables().filter(section=section))
    links = list(Link.objects.enables().filter(section=section))
    files = list(File.objects.enables().filter(section=section))
    profiles = list(ProfilesPage.objects.enables().filter(section=section))
    logos = list(Logos.objects.enables().filter(section=section))

    total = sorted(pages + links + files + profiles + logos, key=lambda x: x.created)

    pages_li = [li_tag.format(x.get_absolute_url(), x.title) for x in total]
    if extra_list:
        pages_li = extra_list + pages_li
    return {'pages': pages_li}


@register.inclusion_tag("pages/footer_menu.html")
def render_footer_menu():
    pages = Page.objects.enables().filter(show_in_footer=True)
    links = Link.objects.enables().filter(show_in_footer=True)

    total = list(pages) + list(links)

    return {'pages': total[:4]}