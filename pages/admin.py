# -*- coding: utf-8 -*-
from django.contrib import admin
from django.utils.safestring import mark_safe
from django.conf import settings
from .models import Page, Link, File, Logos, Logo, ProfilesPage, Profile
from .forms import PageLinkForm


class PageAdmin(admin.ModelAdmin):
    class Media:
        js = (
            '/static/tiny_mce/tinymce.min.js',
            '/static/js/tinymce.js',)

    fieldsets = (
        ('', {
            'fields': ('title', 'subtitle', 'enable', 'full_page'),
        }),
        ('Content', {
            'fields' : ('content',),
        }),
        ('Show in menus', {
            'fields': (('show_in_header', 'section'), 'show_in_footer'),
        }),
    )

    list_display = ('title', 'enable', 'show_in_header', 'show_in_footer',
                    'view_on_site', 'created')
    list_editable = ('enable', 'show_in_header', 'show_in_footer')

    def view_on_site(sef, obj):
        tag = "<a href='{0}' target='_blank'>View on site</a>"
        return mark_safe(tag.format(obj.get_absolute_url()))


class LinkAdmin(admin.ModelAdmin):
    form = PageLinkForm
    fieldsets = (
        ('', {
            'fields': ('title', 'enable'),
        }),
        ('URL', {
            'fields' : ('page', 'use_external_url', 'external_url'),
        }),
        ('Show in menus', {
            'fields': (('show_in_header', 'section'), 'show_in_footer'),
        }),
    )

    list_display = ('title', 'enable', 'show_in_header', 'show_in_footer', 'show_link')
    list_editable = ('enable', 'show_in_header', 'show_in_footer')

    def show_link(sef, obj):
        tag = "<a href='{0}' target='_blank'>Show link</a>"
        return mark_safe(tag.format(obj.get_absolute_url()))


class FileAdmin(admin.ModelAdmin):
    fieldsets = (
        ('', {
            'fields': ('title', 'enable'),
        }),
        ('File', {
            'fields' : ('file_field',),
        }),
        ('Show in menus', {
            'fields': (('show_in_header', 'section'), 'show_in_footer'),
        }),
    )
    list_display = ('title', 'enable', 'show_in_header', 'show_in_footer', 'file_link')

    def file_link(sef, obj):
        tag = "<a href='{0}' target='_blank'>Show link</a>"
        return mark_safe(tag.format(obj.get_absolute_url()))


class LogoInline(admin.TabularInline):
    model = Logo
    extra = 1


class LogosAdmin(admin.ModelAdmin):
    fieldsets = (
        ('', {
            'fields': ('title', 'enable'),
        }),
        ('Show in menus', {
            'fields': (('show_in_header', 'section'), 'show_in_footer'),
        }),
    )
    list_display = ('title', 'enable', 'show_in_header', 'show_in_footer', 'view_on_site')
    inlines = (LogoInline,)

    def view_on_site(sef, obj):
        tag = "<a href='{0}' target='_blank'>View on site</a>"
        return mark_safe(tag.format(obj.get_absolute_url()))


class ProfilesInline(admin.TabularInline):
    model = Profile
    extra = 1

class ProfilesPageAdmin(admin.ModelAdmin):
    class Media:
        js = (
            '/static/tiny_mce/tinymce.min.js',
            '/static/js/tinymce_profiles.js',
        )

    fieldsets = (
        ('', {
            'fields': ('title', 'enable'),
        }),
        ('Show in menus', {
            'fields': (('show_in_header', 'section'), 'show_in_footer'),
        }),
    )
    inlines = (ProfilesInline,)


admin.site.register(Page, PageAdmin)
admin.site.register(Link, LinkAdmin)
admin.site.register(File, FileAdmin)
admin.site.register(Logos, LogosAdmin)
admin.site.register(ProfilesPage, ProfilesPageAdmin)

