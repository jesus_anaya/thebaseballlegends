# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Page.full_page'
        db.add_column(u'pages_page', 'full_page',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)


        # Changing field 'Logo.logo'
        db.alter_column(u'pages_logo', 'logo', self.gf('stdimage.fields.StdImageField')(max_length=100, upload_to='pages/logos/', thumbnail_size={'width': 180, 'force': True, 'height': 135}, size={'width': 194, 'force': None, 'height': 140}))

    def backwards(self, orm):
        # Deleting field 'Page.full_page'
        db.delete_column(u'pages_page', 'full_page')


        # Changing field 'Logo.logo'
        db.alter_column(u'pages_logo', 'logo', self.gf('stdimage.fields.StdImageField')(max_length=100, upload_to='pages/logos/', thumbnail_size={'width': 180, 'force': True, 'height': 135}, size={'width': 600, 'force': None, 'height': 400}))

    models = {
        u'pages.file': {
            'Meta': {'ordering': "('created',)", 'object_name': 'File'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'enable': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'file_field': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'section': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'show_in_footer': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'show_in_header': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '255'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'pages.link': {
            'Meta': {'ordering': "('created',)", 'object_name': 'Link'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'enable': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'external_url': ('django.db.models.fields.URLField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'page': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'section': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'show_in_footer': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'show_in_header': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '255'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'use_external_url': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'pages.logo': {
            'Meta': {'object_name': 'Logo'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo': ('stdimage.fields.StdImageField', [], {'max_length': '100', 'upload_to': "'pages/logos/'", 'thumbnail_size': "{'width': 180, 'force': True, 'height': 135}", 'size': "{'width': 194, 'force': None, 'height': 140}"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'page': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['pages.Logos']"}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        },
        u'pages.logos': {
            'Meta': {'object_name': 'Logos'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'enable': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'section': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'show_in_footer': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'show_in_header': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '255'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'pages.page': {
            'Meta': {'ordering': "('created',)", 'object_name': 'Page'},
            'content': ('django.db.models.fields.TextField', [], {}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'enable': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'full_page': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'section': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'show_in_footer': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'show_in_header': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '255'}),
            'subtitle': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['pages']