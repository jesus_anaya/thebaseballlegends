"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase, LiveServerTestCase
from django.core.urlresolvers import reverse
from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.webdriver.support.ui import WebDriverWait
from .models import Page
import factory


class PageFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = Page
    title = "Page test 1"
    subtitle = "Sub title page"
    content = "<h1>Content of my page</h1>"
    enable = True


class SimpleTest(TestCase):
    def test_model_and_view(self):
        """
        """
        page = PageFactory.create()
        self.assertTrue(page.id is not None)

        response = self.client.get(reverse('page', args=(page.slug,)))
        self.assertEquals(response.status_code, 200)


# class BaseLiveTest(LiveServerTestCase):
#     @classmethod
#     def setUpClass(cls):
#         cls.selenium = WebDriver()
#         super(BaseLiveTest, cls).setUpClass()

#     @classmethod
#     def tearDownClass(cls):
#         super(BaseLiveTest, cls).tearDownClass()
#         cls.selenium.quit()


# class LiveHomeTest(BaseLiveTest):
#     def test_post_submission(self):
#         self.selenium.get(self.live_server_url)
#         WebDriverWait(self.selenium, 10)


