from django.conf.urls import patterns, url
from .views import PageView, LogosView, ProfilesView

urlpatterns = patterns('',
    url(r'^(?P<slug>[-_\w]+)/$', PageView.as_view(), name='page'),
    url(r'^logos/(?P<slug>[-_\w]+)/$', LogosView.as_view(), name='logos_page'),
    url(r'^profiles/(?P<slug>[-_\w]+)/$', ProfilesView.as_view(), name='profiles_page'),
)
