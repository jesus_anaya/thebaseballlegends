from django.views.generic import DetailView
from .models import Page, Logos, Logo, ProfilesPage, Profile


class PageView(DetailView):
    template_name = "pages/page.html"
    queryset = Page.objects.enables()
    context_object_name = 'page'


class LogosView(DetailView):
    template_name = "pages/logos.html"
    queryset = Logos.objects.enables()
    context_object_name = 'page'

    def get_context_data(self, **kwargs):
        context = super(LogosView, self).get_context_data(**kwargs)
        context['logos'] = Logo.objects.filter(page=self.object.id)
        return context


class ProfilesView(DetailView):
    template_name = "pages/profiles.html"
    model = ProfilesPage
    context_object_name = 'page'

    def get_context_data(self, **kwargs):
        context = super(ProfilesView, self).get_context_data(**kwargs)
        context['profiles'] = Profile.objects.filter(page=self.object.id)
        return context
