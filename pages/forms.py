from django import forms
from django.core.urlresolvers import reverse_lazy
from .models import Link, Page

URL_PAGE_CHOICES = [(x.get_absolute_url(), x.title)
                    for x in Page.objects.enables()]

# Add contact page url to choises list
URL_PAGE_CHOICES += [(reverse_lazy('contact'), "Contact us")]


class PageLinkForm(forms.ModelForm):
    class Meta:
        model = Link

    page = forms.ChoiceField(choices=URL_PAGE_CHOICES)