# -*- coding: utf-8 -*-
from django.db import models
from django.core.urlresolvers import reverse_lazy
from django.conf import settings
from website.utils import unique_slug
from website.managers import ContentManager
from stdimage import StdImageField


MENU_SECTIONS = (
    ('about', 'About'),
    ('news', 'News'),
    ('ews', 'Elite World Series'),
    ('tournaments', 'TBL Tournaments'),
    ('multimedia', 'Multimedia'),
    ('league_info', 'League Info'),
    ('live_score', 'Live / Score'),
)


class BasePage(models.Model):
    class Meta:
        ordering = ('created',)
        abstract = True

    title = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255)

    show_in_header = models.BooleanField(default=False)
    show_in_footer = models.BooleanField(default=False)
    section = models.CharField(max_length=50, choices=MENU_SECTIONS,
                                            blank=True, null=True)

    created = models.DateTimeField(auto_now_add=True)
    enable = models.BooleanField(default=True)

    objects = ContentManager()

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return reverse_lazy("page", args=(self.slug,))

    def save(self, *args, **kwargs):
        self.slug = unique_slug(self, 'title', 'slug')
        super(BasePage, self).save(*args, **kwargs)


class Page(BasePage):
    subtitle = models.CharField(max_length=255, blank=True, null=True)
    content = models.TextField()
    full_page = models.BooleanField(default=False)


class Link(BasePage):
    page = models.CharField(max_length=255, blank=True, null=True)

    use_external_url = models.BooleanField(default=False)
    external_url = models.URLField(max_length=500, blank=True, null=True)

    def get_absolute_url(self):
        if self.use_external_url:
            return self.external_url
        else:
            return self.page


class File(BasePage):
    file_field = models.FileField(upload_to='pages/documents/',
                                        verbose_name=u'File')

    def get_absolute_url(self):
        return u"%s%s" % (settings.MEDIA_URL, self.file_field)


class ProfilesPage(BasePage):
    def get_absolute_url(self):
        return reverse_lazy("profiles_page", args=(self.slug,))

class Profile(models.Model):
    page = models.ForeignKey(ProfilesPage)
    name = models.CharField(max_length=200)
    description = models.TextField()
    photo = StdImageField(upload_to="pages/profiles/", blank=True,
        null=True, size=(170, 190), thumbnail_size=(170, 190, True))
    created = models.DateField(auto_now_add=True)

    class Meta:
        ordering = ('created',)

    def __unicode__(self):
        return self.name

    def delete(self):
        self.photo.delete()
        super(Profile, self).delete()


class Logos(BasePage):
    class Meta:
        verbose_name = "Logo page"
        verbose_name_plural = "Logos page"

    def get_absolute_url(self):
        return reverse_lazy("logos_page", args=(self.slug,))


class Logo(models.Model):
    page = models.ForeignKey(Logos)
    name = models.CharField(max_length=255)
    logo = StdImageField(upload_to="pages/logos/", size=(194, 140),
                                    thumbnail_size=(194, 140, False))
    url = models.URLField()
