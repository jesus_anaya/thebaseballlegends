# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from website.sitemap import TBLSitemap
from filebrowser.sites import site as filebrowser_site

admin.autodiscover()

urlpatterns = patterns('',
    # Home and extra pages
    url(r'^', include('website.urls')),

    # internal apps
    url(r'^news/', include('blog.urls')),
    url(r'^page/', include('pages.urls')),
    url(r'^videos/', include('videos.urls')),
    url(r'^gallery/', include('gallery.urls')),
    url(r'^tournament/', include('tournaments.urls')),
    url(r'^panel/', include('panel.urls')),
    url(r'^shop/', include('shop.urls')),

    # local Api RESTful
    url(r'^api/', include('api.urls', namespace='api')),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    # Django Admin
    url(r'^admin/filebrowser/', include(filebrowser_site.urls)),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^register/', include("register.urls")),
    url(r'^inplaceeditform/', include('inplaceeditform.urls')),

    url(r'^sitemap.xml', 'django.contrib.sitemaps.views.sitemap', {
        'sitemaps': {
            'pages': TBLSitemap
        }
    }),

    # Urls pages
    url(r"^", include("pages.urls")),
)

if settings.DEBUG:
    from django.conf.urls.static import static
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
