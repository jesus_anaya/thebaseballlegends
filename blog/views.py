from django.views.generic import DetailView, ListView
from .models import Post

class PostView(DetailView):
    template_name = "blog/post_detail.html"
    context_object_name = "blog_post"
    queryset = Post.objects.enables()


class PostsList(ListView):
    template_name = "blog/posts_list.html"
    context_object_name = "blog_posts"
    queryset = Post.objects.enables()

    def get(self, request, *args, **kwargs):
        if request.GET.get('section'):
            section = request.GET.get('section')
            self.queryset = self.queryset.filter(section=section)
        return super(PostsList, self).get(request, *args, **kwargs)
