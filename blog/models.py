from django.db import models
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse_lazy
from stdimage import StdImageField
from website.utils import unique_slug
from website.managers import ContentManager


class PostCategory(models.Model):
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return self.name


class Post(models.Model):
    class Meta:
        ordering = ['-created']

    title = models.CharField(max_length=200)
    slug = models.SlugField(max_length=255, blank=True)
    enable = models.BooleanField(default=True, blank=True)

    section = models.ForeignKey(PostCategory)

    image = StdImageField(upload_to="blog/originals/", blank=True,
                    size=(600, 400), thumbnail_size=(180, 135, True))

    # Authos Information
    author = models.ForeignKey(User, null=True, blank=True,
        help_text="""Blank this field if you want
                    to assign another author name""")

    author_name = models.CharField(max_length=200, blank=True, null=True)

    # Content data
    body = models.TextField()
    created = models.DateTimeField(auto_now_add=True)

    # Set new Manager
    objects = ContentManager()

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return reverse_lazy("blog_post", args=(self.slug,))

    def save(self, *args, **kwargs):
        self.slug = unique_slug(self, 'title', 'slug')
        super(Post, self).save(*args, **kwargs)
