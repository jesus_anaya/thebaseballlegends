from django.contrib import admin
from django.utils.safestring import mark_safe
from .models import Post, PostCategory

class PostAdmin(admin.ModelAdmin):
    class Media:
        js = (
            '/static/tiny_mce/tinymce.min.js',
            '/static/js/admin/effects.js',
            '/static/js/tinymce.js'
        )

    fieldsets = (
        ('', {
            'fields': ('title', 'section', 'enable'),
        }),
        ('Main Image', {
            'classes': ('wp-collapse',),
            'fields' : ('image',),
        }),
        ('Author', {
            'classes': ('wp-collapse',),
            'fields' : ('author', 'author_name'),
        }),
        ('Content', {
            'classes': ('wp-collapse',),
            'fields' : ('body',),
        }),
    )
    list_display = ('post_image', 'title', 'enable', 'view_on_site', 'created')
    list_editable = ('enable',)

    def post_image(self, obj):
        return mark_safe("<image src='%s' width='100' height='70'>" \
                                        % obj.image.thumbnail.url())

    def view_on_site(sef, obj):
        return mark_safe("<a href='%s' target='_blank'>View on site</a>"\
                                                % obj.get_absolute_url())


admin.site.register(Post, PostAdmin)
admin.site.register(PostCategory)
