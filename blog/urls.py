from django.conf.urls import patterns, url
from .views import PostView, PostsList

urlpatterns = patterns('',
    url(r"^$", PostsList.as_view(), name="blog_posts_list"),
    url(r"^(?P<slug>.*)/$", PostView.as_view(), name="blog_post"),
)