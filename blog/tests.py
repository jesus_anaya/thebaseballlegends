"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from datetime import datetime
import factory
from .models import PostCategory, Post


class PostCategoryFactory(factory.Factory):
    FACTORY_FOR = PostCategory

    id = 10
    name = "Text Category 1123"


class PostFactory(factory.Factory):
    FACTORY_FOR = Post

    id = 1
    title = "Title post test"
    enable = True
    author_name = "Jesus Armando"
    body = "<h1>Ola ke Ase!!!</h1>"
    image = factory.django.ImageField()
    created = datetime.now()


class SimpleTest(TestCase):
    def test_creating_blog_post(self):
        """
        """
        post_category = PostCategoryFactory.create()
        self.assertTrue(post_category.id is not None)

        post = PostFactory.create(section=post_category)
        self.assertTrue(post.id is not None)

