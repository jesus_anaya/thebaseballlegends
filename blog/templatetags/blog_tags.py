from django import template
from django.core.urlresolvers import reverse_lazy
from blog.models import PostCategory as PC
register = template.Library()


@register.simple_tag
def posts_categories_list():
    li_code = "<option value='{0}'>{1}</option>\n" % reverse_lazy(
                                                "blog_posts_list")

    return "".join([li_code.format(x.id, x.name) for x in PC.objects.all()])
